jQuery(document).ready(function() { 
	jQuery('#background-repository').cycle({ 
		fx: 'fade', 
		pager: '#smallnav', 
		pause: 1, 
		speed: 1800, 
		timeout: 10000 
	});	
});