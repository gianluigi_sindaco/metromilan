<%-- _lcid="1033" _version="15.0.4420" _dal="1" --%>
<%-- _LocalBinding --%>
<%@ Page language="C#" MasterPageFile="../_catalogs/masterpage/metromilan_13.master"    Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage,Microsoft.SharePoint,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:webpartpageexpansion="full" meta:progid="SharePoint.WebPartPage.Document"  %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Import Namespace="Microsoft.SharePoint" %> <%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">

<div class="ms-hide">
	<WebPartPages:WebPartZone runat="server" title="loc:TitleBar" id="TitleBar" AllowLayoutChange="false" AllowPersonalization="false" Style="display:none;"><ZoneTemplate>
	<WebPartPages:TitleBarWebPart runat="server" HeaderTitle="all_sc_13" Title="Web Part Page Title Bar" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="TitleBar" PartOrder="2" FrameState="Normal" AllowRemove="False" AllowZoneChange="True" AllowMinimize="False" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_c0a4e01f_c59f_4ac6_a811_0f289eb6c812" AllowClose="False" ChromeType="None" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{C0A4E01F-C59F-4AC6-A811-0F289EB6C812}" WebPart="true" Height="" Width=""></WebPartPages:TitleBarWebPart>

	</ZoneTemplate></WebPartPages:WebPartZone>
  </div>
  <table class="ms-core-tableNoSpace ms-webpartPage-root" width="100%">
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Header" ID="Header" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:LeftColumn" ID="LeftColumn" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:MiddleColumn" ID="MiddleColumn" FrameType="TitleBarOnly"><ZoneTemplate>
<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" NoDefaultStyle="" ViewGuid="{476EAC49-0E1C-4C9B-A10B-560477EEF989}" EnableOriginalValue="False" ViewContentTypeId="0x" ListUrl="" ListDisplayName="" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Tutte le schede contenuto" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="2" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_476eac49_0e1c_4c9b_a10b_560477eef989" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{476EAC49-0E1C-4C9B-A10B-560477EEF989}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
						<ParameterBinding Name="category" Location="QueryString(category)" DefaultValue="###" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.xsl</XslLink>
<XmlDefinition>
						<View Name="{476EAC49-0E1C-4C9B-A10B-560477EEF989}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/all_sc.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
						
							<Query>
								<OrderBy>
									<FieldRef Name="Modified" Ascending="FALSE"/>
						
								</OrderBy>
								<Where>
									<And>
										<And>
											<Or>
												<Eq>
													<FieldRef Name="search_all"/>
						
													<Value Type="Text">{category}</Value>
												</Eq>
												<Eq>
													<FieldRef Name="IdCategoria"/>
						
													<Value Type="Text">{category}</Value>
												</Eq>
											</Or>
											<Eq>
												<FieldRef Name="_ModerationStatus"/>
						
												<Value Type="ModStat">0</Value>
											</Eq>
										</And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto"/>
						
											<Value Type="Text">Scheda Contenuto</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="body"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="link_1"/>
								<FieldRef Name="link_2"/>
								<FieldRef Name="link_3"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
						
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="search_all"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="IconaCategoria"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="stile_box"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="subtitle"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
						
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
						
										<FieldRef List="categorie" Name="ID"/>
						
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
						
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
						
								<Field Name="IconaCategoria" Type="Lookup" List="categorie" ShowField="Icona"/>
						
							</ProjectedFields>
							<RowLimit Paged="TRUE">30</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Standard"/>
						</View>
					</XmlDefinition>
<xsl><xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:pcm="urn:PageContentManager" xmlns:ddwrt2="urn:frontpage:internal" xmlns:o="urn:schemas-microsoft-com:office:office"> 
  <xsl:include href="/_layouts/15/xsl/main.xsl"/> 
  <xsl:include href="/_layouts/15/xsl/internal.xsl"/> 
  						<xsl:param name="AllRows" select="/dsQueryResponse/Rows/Row[$EntityName = '' or (position() &gt;= $FirstRow and position() &lt;= $LastRow)]"/>
  						<xsl:param name="dvt_apos">'</xsl:param>
						<xsl:template name="View_Default_RootTemplate" mode="RootTemplate" match="View" ddwrt:dvt_mode="root" ddwrt:ghost="" xmlns:ddwrt2="urn:frontpage:internal">
    						<xsl:param name="ShowSelectAllCheckbox" select="'True'"/>
    						<xsl:if test="($IsGhosted = '0' and $MasterVersion=3 and Toolbar[@Type='Standard']) or $ShowAlways">
      							<xsl:call-template name="ListViewToolbar"/>
    </xsl:if>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      	<xsl:if test="not($NoCTX)">
        	<xsl:call-template name="CTXGeneration"/>
      </xsl:if>
      	<xsl:if test="List/@TemplateType=109 and not($ClientRender='1')">
        	<xsl:call-template name="PicLibScriptGeneration"/>
      </xsl:if>
      <tr>
        <td id="script{$ContainerId}">
          	<xsl:if test="not($ClientRender='1')">
            	<xsl:if test="not($NoAJAX)">
              <iframe src="javascript:false;" id="FilterIframe{$ViewCounter}" name="FilterIframe{$ViewCounter}" style="display:none" height="0" width="0" FilterLink="{$FilterLink}"></iframe>
            </xsl:if>
            <table summary="{List/@title} {List/@description}" o:WebQuerySourceHref="{$HttpPath}&amp;XMLDATA=1&amp;RowLimit=0&amp;View={$View}"
                            width="100%" border="0" cellspacing="0" dir="{List/@Direction}">
              	<xsl:if test="not($NoCTX)">
                	<xsl:attribute name="onmouseover">
                  EnsureSelectionHandler(event,this,<xsl:value-of select ="$ViewCounter"/>)
                </xsl:attribute>
              </xsl:if>
              	<xsl:if test="$NoAJAX">
                	<xsl:attribute name="FilterLink">
                  <xsl:value-of select="$FilterLink"/>
                </xsl:attribute>
              </xsl:if>
              	<xsl:attribute name="cellpadding">
                	<xsl:choose>
                  		<xsl:when test="ViewStyle/@ID='15' or ViewStyle/@ID='16'">0</xsl:when>
                  		<xsl:otherwise>1</xsl:otherwise>
                	</xsl:choose>
              </xsl:attribute>
              	<xsl:attribute name="id">
                	<xsl:choose>
                  		<xsl:when test="$IsDocLib or dvt_RowCount = 0">onetidDoclibViewTbl0</xsl:when>
                  		<xsl:otherwise>
                    <xsl:value-of select="concat($List, '-', $View)"/>
                  </xsl:otherwise>
                	</xsl:choose>
              </xsl:attribute>
              	<xsl:attribute name="class">
                	<xsl:choose>
                  		<xsl:when test="ViewStyle/@ID='0' or ViewStyle/@ID='17'">
                    <xsl:value-of select="$ViewClassName"/> ms-basictable
                  </xsl:when>
                  		<xsl:otherwise>
                    <xsl:value-of select="$ViewClassName"/>
                  </xsl:otherwise>
                	</xsl:choose>
              </xsl:attribute>
              	<xsl:if test="$InlineEdit">
                	<xsl:attribute name="inlineedit">
                  javascript: <xsl:value-of select="ddwrt:GenFireServerEvent('__cancel;dvt_form_key={@ID}')"/>;CoreInvoke('ExpGroupOnPageLoad', 'true');
                </xsl:attribute>
              </xsl:if>
              <xsl:apply-templates select="." mode="full">
                <xsl:with-param name="ShowSelectAllCheckbox" select="$ShowSelectAllCheckbox"/>
              </xsl:apply-templates>
            </table>
          </xsl:if>          
        </td>
      </tr>
      	<xsl:if test="not($ClientRender='1') and $dvt_RowCount = 0 and not (@BaseViewID='3' and List/@TemplateType='102')">
        <tr>
          <td>
             <table width="100%" border="0" dir="{List/@Direction}">
               	<xsl:call-template name="EmptyTemplate" />
             </table>
          </td>
        </tr>
      </xsl:if>
    </table>
    						<xsl:choose>
        						<xsl:when test="$ClientRender='1'">
           <div id="scriptPaging{$ContainerId}"/>
        </xsl:when>
        						<xsl:otherwise>
        							<xsl:call-template name="pagingButtons" />
        							<xsl:if test="Toolbar[@Type='Freeform'] or ($MasterVersion>=4 and Toolbar[@Type='Standard'])">
          								<xsl:call-template name="Freeform">
            								<xsl:with-param name="AddNewText">
              									<xsl:choose>
                									<xsl:when test="List/@TemplateType='104'">
                  <xsl:value-of select="'Add new announcement'"/>
                </xsl:when>
                									<xsl:when test="List/@TemplateType='101' or List/@TemplateType='115'">
                  <xsl:value-of select="'Add document'"/>
                </xsl:when>
                									<xsl:when test="List/@TemplateType='103'">
                  <xsl:value-of select="'Add new link'"/>
                </xsl:when>
                									<xsl:when test="List/@TemplateType='106'">
                  <xsl:value-of select="'Add new event'"/>
                </xsl:when>
                									<xsl:when test="List/@TemplateType='119'">
                  <xsl:value-of select="'Add new page'"/>
                </xsl:when>
                									<xsl:otherwise>
                  <xsl:value-of select="'Add new item'"/>
                </xsl:otherwise>
              									</xsl:choose>
            </xsl:with-param>
            								<xsl:with-param name="ID">
              									<xsl:choose>
              										<xsl:when test="List/@TemplateType='104'">idHomePageNewAnnouncement</xsl:when>
              										<xsl:when test="List/@TemplateType='101'">idHomePageNewDocument</xsl:when>
              										<xsl:when test="List/@TemplateType='103'">idHomePageNewLink</xsl:when>
              										<xsl:when test="List/@TemplateType='106'">idHomePageNewEvent</xsl:when>
              										<xsl:when test="List/@TemplateType='119'">idHomePageNewWikiPage</xsl:when>
              										<xsl:otherwise>idHomePageNewItem</xsl:otherwise>
              									</xsl:choose>
            </xsl:with-param>
          								</xsl:call-template>
        </xsl:if>
        </xsl:otherwise>
    						</xsl:choose>
  </xsl:template></xsl:stylesheet></xsl></WebPartPages:XsltListViewWebPart>					
					</ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:RightColumn" ID="RightColumn" FrameType="TitleBarOnly"><ZoneTemplate>
<WebPartPages:ContentEditorWebPart runat="server" __MarkupType="xmlmarkup" WebPart="true" __WebPartId="{B12EDB79-F72C-4F87-A83D-4702A4E2B514}" >
<WebPart xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.microsoft.com/WebPart/v2"><Title>Content Editor</Title>
  <FrameType>None</FrameType>
  <Description>Allows authors to enter rich text content.</Description>
  <IsIncluded>true</IsIncluded>
  <PartOrder>2</PartOrder>
  <FrameState>Normal</FrameState>
  <Height />
  <Width />
  <AllowRemove>true</AllowRemove>
  <AllowZoneChange>true</AllowZoneChange>
  <AllowMinimize>true</AllowMinimize>
  <AllowConnect>true</AllowConnect>
  <AllowEdit>true</AllowEdit>
  <AllowHide>true</AllowHide>
  <IsVisible>true</IsVisible>
  <DetailLink />
  <HelpLink />
  <HelpMode>Modeless</HelpMode>
  <Dir>Default</Dir>
  <PartImageSmall />
  <MissingAssembly>Cannot import this Web Part.</MissingAssembly>
  <PartImageLarge>/_layouts/images/mscontl.gif</PartImageLarge>
  <IsIncludedFilter />
  <ExportControlledProperties>true</ExportControlledProperties>
  <ConnectionID>00000000-0000-0000-0000-000000000000</ConnectionID>
  <ID>g_b12edb79_f72c_4f87_a83d_4702a4e2b514</ID>
  <ContentLink xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor" />
  <Content xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor"><![CDATA[<div class="category-container">  
    <div class="category-row">
      <div class="category-cell">
        <a href="/SitePages/all_news_old.aspx">
          Tutte le News
        </a>
      </div>
    </div>
</div>
]]></Content>
  <PartStorage xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor" />
</WebPart>
</WebPartPages:ContentEditorWebPart>
<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{A9280F83-49A6-4277-9D15-84ADE3B581C4}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{40295D79-3C61-41EE-BB17-69495FE28FCA}" ListId="40295d79-3c61-41ee-bb17-69495fe28fca" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Categorie" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="4" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Categorie" DetailLink="/Lists/Categorie" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_a9280f83_49a6_4277_9d15_84ade3b581c4" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{A9280F83-49A6-4277-9D15-84ADE3B581C4}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.categories.xsl</XslLink>
<XmlDefinition>
						<View Name="{A9280F83-49A6-4277-9D15-84ADE3B581C4}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/all_sc.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
						
							<Query>
								<OrderBy>
									<FieldRef Name="ID"/>
								</OrderBy>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="Descrizione"/>
								<FieldRef Name="Codice"/>
							</ViewFields>
							<RowLimit Paged="TRUE">30</RowLimit>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
<xsl><xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:pcm="urn:PageContentManager" xmlns:ddwrt2="urn:frontpage:internal" xmlns:o="urn:schemas-microsoft-com:office:office"> 
  <xsl:include href="/_layouts/15/xsl/main.xsl"/> 
  <xsl:include href="/_layouts/15/xsl/internal.xsl"/> 
  						<xsl:param name="AllRows" select="/dsQueryResponse/Rows/Row[$EntityName = '' or (position() &gt;= $FirstRow and position() &lt;= $LastRow)]"/>
  						<xsl:param name="dvt_apos">'</xsl:param>
						<xsl:template name="View_Default_RootTemplate" mode="RootTemplate" match="View" ddwrt:dvt_mode="root" ddwrt:ghost="" xmlns:ddwrt2="urn:frontpage:internal">
    						<xsl:param name="ShowSelectAllCheckbox" select="'True'"/>
    						<xsl:if test="($IsGhosted = '0' and $MasterVersion=3 and Toolbar[@Type='Standard']) or $ShowAlways">
      							<xsl:call-template name="ListViewToolbar"/>
    </xsl:if>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      	<xsl:if test="not($NoCTX)">
        	<xsl:call-template name="CTXGeneration"/>
      </xsl:if>
      	<xsl:if test="List/@TemplateType=109 and not($ClientRender='1')">
        	<xsl:call-template name="PicLibScriptGeneration"/>
      </xsl:if>
      <tr>
        <td id="script{$ContainerId}">
          	<xsl:if test="not($ClientRender='1')">
            	<xsl:if test="not($NoAJAX)">
              <iframe src="javascript:false;" id="FilterIframe{$ViewCounter}" name="FilterIframe{$ViewCounter}" style="display:none" height="0" width="0" FilterLink="{$FilterLink}"></iframe>
            </xsl:if>
            <table summary="{List/@title} {List/@description}" o:WebQuerySourceHref="{$HttpPath}&amp;XMLDATA=1&amp;RowLimit=0&amp;View={$View}"
                            width="100%" border="0" cellspacing="0" dir="{List/@Direction}">
              	<xsl:if test="not($NoCTX)">
                	<xsl:attribute name="onmouseover">
                  EnsureSelectionHandler(event,this,<xsl:value-of select ="$ViewCounter"/>)
                </xsl:attribute>
              </xsl:if>
              	<xsl:if test="$NoAJAX">
                	<xsl:attribute name="FilterLink">
                  <xsl:value-of select="$FilterLink"/>
                </xsl:attribute>
              </xsl:if>
              	<xsl:attribute name="cellpadding">
                	<xsl:choose>
                  		<xsl:when test="ViewStyle/@ID='15' or ViewStyle/@ID='16'">0</xsl:when>
                  		<xsl:otherwise>1</xsl:otherwise>
                	</xsl:choose>
              </xsl:attribute>
              	<xsl:attribute name="id">
                	<xsl:choose>
                  		<xsl:when test="$IsDocLib or dvt_RowCount = 0">onetidDoclibViewTbl0</xsl:when>
                  		<xsl:otherwise>
                    <xsl:value-of select="concat($List, '-', $View)"/>
                  </xsl:otherwise>
                	</xsl:choose>
              </xsl:attribute>
              	<xsl:attribute name="class">
                	<xsl:choose>
                  		<xsl:when test="ViewStyle/@ID='0' or ViewStyle/@ID='17'">
                    <xsl:value-of select="$ViewClassName"/> ms-basictable
                  </xsl:when>
                  		<xsl:otherwise>
                    <xsl:value-of select="$ViewClassName"/>
                  </xsl:otherwise>
                	</xsl:choose>
              </xsl:attribute>
              	<xsl:if test="$InlineEdit">
                	<xsl:attribute name="inlineedit">
                  javascript: <xsl:value-of select="ddwrt:GenFireServerEvent('__cancel;dvt_form_key={@ID}')"/>;CoreInvoke('ExpGroupOnPageLoad', 'true');
                </xsl:attribute>
              </xsl:if>
              <xsl:apply-templates select="." mode="full">
                <xsl:with-param name="ShowSelectAllCheckbox" select="$ShowSelectAllCheckbox"/>
              </xsl:apply-templates>
            </table>
          </xsl:if>          
        </td>
      </tr>
      	<xsl:if test="not($ClientRender='1') and $dvt_RowCount = 0 and not (@BaseViewID='3' and List/@TemplateType='102')">
        <tr>
          <td>
             <table width="100%" border="0" dir="{List/@Direction}">
               	<xsl:call-template name="EmptyTemplate" />
             </table>
          </td>
        </tr>
      </xsl:if>
    </table>
    						<xsl:choose>
        						<xsl:when test="$ClientRender='1'">
           <div id="scriptPaging{$ContainerId}"/>
        </xsl:when>
        						<xsl:otherwise>
        							<xsl:call-template name="pagingButtons" />
        							<xsl:if test="Toolbar[@Type='Freeform'] or ($MasterVersion>=4 and Toolbar[@Type='Standard'])">
          								<xsl:call-template name="Freeform">
            								<xsl:with-param name="AddNewText">
              									<xsl:choose>
                									<xsl:when test="List/@TemplateType='104'">
                  <xsl:value-of select="'Add new announcement'"/>
                </xsl:when>
                									<xsl:when test="List/@TemplateType='101' or List/@TemplateType='115'">
                  <xsl:value-of select="'Add document'"/>
                </xsl:when>
                									<xsl:when test="List/@TemplateType='103'">
                  <xsl:value-of select="'Add new link'"/>
                </xsl:when>
                									<xsl:when test="List/@TemplateType='106'">
                  <xsl:value-of select="'Add new event'"/>
                </xsl:when>
                									<xsl:when test="List/@TemplateType='119'">
                  <xsl:value-of select="'Add new page'"/>
                </xsl:when>
                									<xsl:otherwise>
                  <xsl:value-of select="'Add new item'"/>
                </xsl:otherwise>
              									</xsl:choose>
            </xsl:with-param>
            								<xsl:with-param name="ID">
              									<xsl:choose>
              										<xsl:when test="List/@TemplateType='104'">idHomePageNewAnnouncement</xsl:when>
              										<xsl:when test="List/@TemplateType='101'">idHomePageNewDocument</xsl:when>
              										<xsl:when test="List/@TemplateType='103'">idHomePageNewLink</xsl:when>
              										<xsl:when test="List/@TemplateType='106'">idHomePageNewEvent</xsl:when>
              										<xsl:when test="List/@TemplateType='119'">idHomePageNewWikiPage</xsl:when>
              										<xsl:otherwise>idHomePageNewItem</xsl:otherwise>
              									</xsl:choose>
            </xsl:with-param>
          								</xsl:call-template>
        </xsl:if>
        </xsl:otherwise>
    						</xsl:choose>
  </xsl:template></xsl:stylesheet></xsl></WebPartPages:XsltListViewWebPart>					
					</ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Footer" ID="Footer" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<SharePoint:ScriptBlock runat="server">
				if(typeof(MSOLayout_MakeInvisibleIfEmpty) == &quot;function&quot;) 
				{MSOLayout_MakeInvisibleIfEmpty();}</SharePoint:ScriptBlock>
		</table>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderSearchArea" runat="server">
	<SharePoint:DelegateControl runat="server"
		ControlId="SmallSearchInputBox"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageDescription" runat="server">
	<SharePoint:ProjectProperty Property="Description" runat="server"/>
</asp:Content>

