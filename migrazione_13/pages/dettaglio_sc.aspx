<%-- _lcid="1033" _version="15.0.4420" _dal="1" --%>
<%-- _LocalBinding --%>
<%@ Page language="C#" MasterPageFile="../_catalogs/masterpage/metromilan_13.master"    Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage,Microsoft.SharePoint,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" meta:webpartpageexpansion="full"  %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Import Namespace="Microsoft.SharePoint" %> <%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
<div class="ms-hide">
	<WebPartPages:WebPartZone runat="server" title="loc:TitleBar" id="TitleBar" AllowLayoutChange="false" AllowPersonalization="false" Style="display:none;"><ZoneTemplate>
	<WebPartPages:TitleBarWebPart runat="server" HeaderTitle="dettaglio_sc" Title="Web Part Page Title Bar" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="TitleBar" PartOrder="2" FrameState="Normal" AllowRemove="False" AllowZoneChange="True" AllowMinimize="False" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_00d1e42e_d12a_4ccf_8f2e_da569bff0b36" AllowClose="False" ChromeType="None" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{00D1E42E-D12A-4CCF-8F2E-DA569BFF0B36}" WebPart="true" Height="" Width=""></WebPartPages:TitleBarWebPart>

	</ZoneTemplate></WebPartPages:WebPartZone>
  </div>
  <table class="ms-core-tableNoSpace ms-webpartPage-root" width="100%">
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Header" ID="Header" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:LeftColumn" ID="LeftColumn" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:MiddleColumn" ID="MiddleColumn" FrameType="TitleBarOnly"><ZoneTemplate>
					<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" NoDefaultStyle="" ViewGuid="{07183B4F-6187-47C5-9D0F-9EA27D9CB355}" EnableOriginalValue="False" ViewContentTypeId="0x" ListUrl="" ListDisplayName="" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Scheda_Contenuto" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="2" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_07183b4f_6187_47c5_9d0f_9ea27d9cb355" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{07183B4F-6187-47C5-9D0F-9EA27D9CB355}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
						<ParameterBinding Name="SC_ID" Location="QueryString(SC_ID)" DefaultValue="" />
						<ParameterBinding Name="UserID" Location="ServerVariable(LOGON_USER)" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.sc.detail.xsl</XslLink>
<XmlDefinition>
						<View Name="{07183B4F-6187-47C5-9D0F-9EA27D9CB355}" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/dettaglio_sc.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<Where>
									<Eq>
										<FieldRef Name="ID"/>
										<Value Type="Integer">{SC_ID}</Value>
									</Eq>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="body"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="link_1"/>
								<FieldRef Name="link_2"/>
								<FieldRef Name="link_3"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="search_all"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="IconaCategoria"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="Editor"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="icona"/>
								<FieldRef Name="Author"/>
								<FieldRef Name="firma"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="tipo_feedback"/>
								<FieldRef Name="Attachments"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
								<Join Type="LEFT" ListAlias="firme">
									<Eq>
										<FieldRef Name="firma" RefType="Id"/>
										<FieldRef List="firme" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
								<Field Name="IconaCategoria" Type="Lookup" List="categorie" ShowField="Icona"/>
								<Field Name="icona" Type="Lookup" List="firme" ShowField="icona"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">1</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="AssetPicker.xsl" NoDefaultStyle="" ViewGuid="{98A7CB9F-0F37-4D42-B429-D8EFD59D5F31}" EnableOriginalValue="False" ViewContentTypeId="0x" ListUrl="" ListDisplayName="" ListName="{71182E35-FA11-4DB9-B090-5A37E368B973}" ListId="71182e35-fa11-4db9-b090-5a37e368b973" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" FireInitialRow="False" Title="Immagini Allegate" FrameType="None" SuppressWebPartChrome="False" Description="This system library was created by the Publishing Resources feature to store images that are used throughout the site collection." IsIncluded="True" PartOrder="4" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itdl.png" TitleUrl="/SiteCollectionImages" DetailLink="/SiteCollectionImages" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itdl.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_98a7cb9f_0f37_4d42_b429_d8efd59d5f31" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{98A7CB9F-0F37-4D42-B429-D8EFD59D5F31}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="Viewing" Location="Form(APDView)" DefaultValue="Thumbs" />
						<ParameterBinding Name="ThumbStyle" Location="Form(APDThumbNailStyle)" DefaultValue="medium" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="img_folder" Location="QueryString(img_folder)" />
					</ParameterBindings>
<Xsl>
<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal" xmlns:o="urn:schemas-microsoft-com:office:office"> 
  <xsl:include href="/_layouts/xsl/AssetPicker.xsl"/> 
  <xsl:include href="/_layouts/xsl/internal.xsl"/> 
  						<xsl:param name="AllRows" select="/dsQueryResponse/Rows/Row[$EntityName = '' or (position() &gt;= $FirstRow and position() &lt;= $LastRow)]"/>
  						<xsl:param name="dvt_apos">&apos;</xsl:param>
						<xsl:template name="View_Default_RootTemplate" mode="RootTemplate" match="View" ddwrt:dvt_mode="root" ddwrt:ghost="" xmlns:ddwrt2="urn:frontpage:internal">
    						<xsl:param name="ShowSelectAllCheckbox" select="'True'"/>
    						<xsl:if test="($IsGhosted = '0' and $MasterVersion=3 and Toolbar[@Type='Standard']) or $ShowAlways">
      							<xsl:call-template name="ListViewToolbar"/>
    </xsl:if>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
      	<xsl:if test="not($NoCTX)">
        	<xsl:call-template name="CTXGeneration"/>
      </xsl:if>
      	<xsl:if test="List/@TemplateType=109">
        	<xsl:call-template name="PicLibScriptGeneration"/>
      </xsl:if>
      <tr>
        <td>
          	<xsl:if test="not($NoAJAX)">
            <iframe src="javascript:false;" id="FilterIframe{$ViewCounter}" name="FilterIframe{$ViewCounter}" style="display:none" height="0" width="0" FilterLink="{$FilterLink}"></iframe>
          </xsl:if>
          <table summary="{List/@title} {List/@description}" o:WebQuerySourceHref="{$HttpPath}&amp;XMLDATA=1&amp;RowLimit=0&amp;View={$View}" 
                          width="100%" border="0" cellspacing="0" dir="{List/@Direction}">
            <xsl:if test="not($NoCTX)">
              	<xsl:attribute name="onmouseover">EnsureSelectionHandler(event,this,<xsl:value-of select ="$ViewCounter"/>)</xsl:attribute>
            </xsl:if>
            <xsl:if test="$NoAJAX">
              	<xsl:attribute name="FilterLink">
                <xsl:value-of select="$FilterLink"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name="cellpadding">
              	<xsl:choose>
                	<xsl:when test="ViewStyle/@ID='15' or ViewStyle/@ID='16'">0</xsl:when>
                	<xsl:otherwise>1</xsl:otherwise>
              	</xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="id">
              	<xsl:choose>
                	<xsl:when test="$IsDocLib or dvt_RowCount = 0">onetidDoclibViewTbl0</xsl:when>
                	<xsl:otherwise>
                  <xsl:value-of select="concat($List, '-', $View)"/>
                </xsl:otherwise>
              	</xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="class">
              	<xsl:choose>
                	<xsl:when test="ViewStyle/@ID='0' or ViewStyle/@ID='17'"><xsl:value-of select="$ViewClassName"/> ms-basictable</xsl:when>
                	<xsl:when test="ViewStyle/@ID='16'"><xsl:value-of select="$ViewClassName"/> ms-listviewtable2</xsl:when>
                	<xsl:otherwise><xsl:value-of select="$ViewClassName"/></xsl:otherwise>
              	</xsl:choose>
            </xsl:attribute>
            <xsl:if test="$InlineEdit">
              	<xsl:attribute name="inlineedit">javascript: <xsl:value-of select="ddwrt:GenFireServerEvent('__cancel;dvt_form_key={@ID}')"/>;CoreInvoke(&apos;ExpGroupOnPageLoad&apos;, &apos;true&apos;);</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="." mode="full">
              	<xsl:with-param name="ShowSelectAllCheckbox" select="$ShowSelectAllCheckbox"/>
            </xsl:apply-templates>
          </table>
              <xsl:choose>
                <xsl:when test="$IsDocLib or dvt_RowCount = 0"><script type='text/javascript'>HideListViewRows("onetidDoclibViewTbl0");</script></xsl:when>
                <xsl:otherwise>
                  <script type='text/javascript'><xsl:value-of select ="concat('HideListViewRows(&quot;', $List, '-', $View, '&quot;);')"/></script>
                </xsl:otherwise>
              </xsl:choose>
        </td>
      </tr>
      	<xsl:if test="$dvt_RowCount = 0 and not (@BaseViewID='3' and List/@TemplateType='102')">
        <tr>
          <td>
             <table width="100%" border="0" dir="{List/@Direction}">
               	<xsl:call-template name="EmptyTemplate" />
             </table>
          </td>
        </tr>
      </xsl:if>
    </table>
    						<xsl:call-template name="pagingButtons" />
    						<xsl:if test="Toolbar[@Type='Freeform'] or ($MasterVersion=4 and Toolbar[@Type='Standard'])">
      							<xsl:call-template name="Freeform">
        							<xsl:with-param name="AddNewText">
          								<xsl:choose>
            								<xsl:when test="List/@TemplateType='104'">
              <xsl:value-of select="'Add new announcement'"/>
            </xsl:when>
            								<xsl:when test="List/@TemplateType='101' or List/@TemplateType='115'">
              <xsl:value-of select="'Add document'"/>
            </xsl:when>
            								<xsl:when test="List/@TemplateType='103'">
              <xsl:value-of select="'Add new link'"/>
            </xsl:when>
            								<xsl:when test="List/@TemplateType='106'">
              <xsl:value-of select="'Add new event'"/>
            </xsl:when>
            								<xsl:when test="List/@TemplateType='119'">
              <xsl:value-of select="'Add new page'"/>
            </xsl:when>
            								<xsl:otherwise>
              <xsl:value-of select="'Add new item'"/>
            </xsl:otherwise>
          								</xsl:choose>
        </xsl:with-param>
        							<xsl:with-param name="ID">
          								<xsl:choose>
          									<xsl:when test="List/@TemplateType='104'">idHomePageNewAnnouncement</xsl:when>
          									<xsl:when test="List/@TemplateType='101'">idHomePageNewDocument</xsl:when>
          									<xsl:when test="List/@TemplateType='103'">idHomePageNewLink</xsl:when>
          									<xsl:when test="List/@TemplateType='106'">idHomePageNewEvent</xsl:when>
          									<xsl:when test="List/@TemplateType='119'">idHomePageNewWikiPage</xsl:when>
          									<xsl:otherwise>idHomePageNewItem</xsl:otherwise>
          								</xsl:choose>
        </xsl:with-param>
      							</xsl:call-template>
    </xsl:if>
  </xsl:template></xsl:stylesheet></Xsl>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.imgs.xsl</XslLink>
<XmlDefinition>
						<View Name="{98A7CB9F-0F37-4D42-B429-D8EFD59D5F31}" MobileView="TRUE" Type="HTML" Hidden="TRUE" Scope="Recursive" DisplayName="" Url="/SitePages/dettaglio_sc.aspx" Level="1" BaseViewID="40" ContentTypeID="0x" ImageUrl="/_layouts/15/images/dlicon.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="FileLeafRef"/>
								</OrderBy>
								<Where>
									<Eq>
										<FieldRef Name="FileDirRef"/>
										<Value Type="Text">{img_folder}</Value>
									</Eq>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="LinkFilename"/>
								<FieldRef Name="Title"/>
								<FieldRef Name="ContentType"/>
								<FieldRef Name="PreviewOnForm"/>
								<FieldRef Name="ThumbnailOnForm"/>
								<FieldRef Name="PublishingStartDate"/>
								<FieldRef Name="PublishingExpirationDate"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="Editor"/>
							</ViewFields>
							<RowLimit Paged="TRUE">100</RowLimit>
							<Aggregations Value="Off"/>
							<ViewStyle ID="0"/>
							<JSLink>clienttemplates.js|SP.UI.TileView.js|SP.UI.AssetLibrary.js|callout.js</JSLink>
							<XslLink>AssetPicker.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{D5C0B2E1-9E76-42C9-B36C-76E49197FC6B}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{CF9FB0C9-77E4-4A22-8A8C-5A193FC18EC0}" ListId="cf9fb0c9-77e4-4a22-8a8c-5a193fc18ec0" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Documenti Utili" FrameType="None" SuppressWebPartChrome="False" Description="This system library was created by the Publishing Resources feature to store documents that are used throughout the site collection." IsIncluded="True" PartOrder="6" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itdl.png" TitleUrl="/SiteCollectionDocuments" DetailLink="/SiteCollectionDocuments" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itdl.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_d5c0b2e1_9e76_42c9_b36c_76e49197fc6b" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{D5C0B2E1-9E76-42C9-B36C-76E49197FC6B}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noitemsinview_doclibrary)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noitemsinview_doclibrary_howto2)" />
						<ParameterBinding Name="doc_folder" Location="QueryString(doc_folder)" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.docs.xsl</XslLink>
<XmlDefinition>
						<View Name="{D5C0B2E1-9E76-42C9-B36C-76E49197FC6B}" MobileView="TRUE" Type="HTML" Hidden="TRUE" Scope="Recursive" DisplayName="" Url="/SitePages/dettaglio_sc.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/dlicon.png?rev=23" >
							<Query>
								<Where>
									<Eq>
										<FieldRef Name="FileDirRef"/>
										<Value Type="Text">{doc_folder}</Value>
									</Eq>
								</Where>
								<OrderBy>
									<FieldRef Name="FileLeafRef"/>
								</OrderBy>
							</Query>
							<ViewFields>
								<FieldRef Name="DocIcon"/>
								<FieldRef Name="LinkFilename"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="Editor"/>
							</ViewFields>
							<RowLimit Paged="TRUE">30</RowLimit>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

					</ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:RightColumn" ID="RightColumn" FrameType="TitleBarOnly"><ZoneTemplate>
					<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" NoDefaultStyle="" ViewGuid="{E2C2D37F-E98F-47FF-AECF-2E10EC2720A7}" EnableOriginalValue="False" ViewContentTypeId="0x" ListUrl="" ListDisplayName="" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Scheda_Contenuto" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="2" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_e2c2d37f_e98f_47ff_aecf_2e10ec2720a7" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{E2C2D37F-E98F-47FF-AECF-2E10EC2720A7}" __AllowXSLTEditing="true" WebPart="true" Height="" Width="" __designer:customxsl="fldtypes_Ratings.xsl"><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
						<ParameterBinding Name="area" Location="QueryString(area)" DefaultValue="" />
						<ParameterBinding Name="SC_ID" Location="QueryString(SC_ID)" DefaultValue="" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.sc.sidebox.xsl</XslLink>
<XmlDefinition>
						<View Name="{E2C2D37F-E98F-47FF-AECF-2E10EC2720A7}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/dettaglio_sc.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<Where>
									<And>
										<And>
											<And>
												<Eq>
													<FieldRef Name="tipologia_scheda_contenuto"/>
													<Value Type="Text">Scheda Contenuto</Value>
												</Eq>
												<Eq>
													<FieldRef Name="_ModerationStatus"/>
													<Value Type="ModStat">0</Value>
												</Eq>
											</And>
											<Eq>
												<FieldRef Name="Area"/>
												<Value Type="Text">{area}</Value>
											</Eq>
										</And>
										<Neq>
											<FieldRef Name="ID"/>
											<Value Type="Text">{SC_ID}</Value>
										</Neq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="IconaCategoria"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="subtitle"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="Modified"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
								<Field Name="IconaCategoria" Type="Lookup" List="categorie" ShowField="Icona"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">3</RowLimit>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{E76BBC0E-2C35-4981-B74B-2D1DDED4474B}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Scheda_Contenuto" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="4" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_e76bbc0e_2c35_4981_b74b_2d1dded4474b" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{E76BBC0E-2C35-4981-B74B-2D1DDED4474B}" __AllowXSLTEditing="true" WebPart="true" Height="" Width="" __designer:customxsl="fldtypes_Ratings.xsl"><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
						<ParameterBinding Name="area" Location="QueryString(area)" DefaultValue="" />
						<ParameterBinding Name="SC_ID" Location="QueryString(SC_ID)" DefaultValue="" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.sc.sidebox.xsl</XslLink>
<XmlDefinition>
						<View Name="{E76BBC0E-2C35-4981-B74B-2D1DDED4474B}" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/dettaglio_sc.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<Where>
									<And>
										<And>
											<And>
												<Eq>
													<FieldRef Name="tipologia_scheda_contenuto"/>
													<Value Type="Text">News</Value>
												</Eq>
												<Eq>
													<FieldRef Name="_ModerationStatus"/>
													<Value Type="ModStat">0</Value>
												</Eq>
											</And>
											<Eq>
												<FieldRef Name="Area"/>
												<Value Type="Text">{area}</Value>
											</Eq>
										</And>
										<Neq>
											<FieldRef Name="ID"/>
											<Value Type="Text">{SC_ID}</Value>
										</Neq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="IconaCategoria"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="subtitle"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="Modified"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
								<Field Name="IconaCategoria" Type="Lookup" List="categorie" ShowField="Icona"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">3</RowLimit>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

					</ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Footer" ID="Footer" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<SharePoint:ScriptBlock runat="server">
				if(typeof(MSOLayout_MakeInvisibleIfEmpty) == &quot;function&quot;) 
				{MSOLayout_MakeInvisibleIfEmpty();}</SharePoint:ScriptBlock>
		</table>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderSearchArea" runat="server">
	<SharePoint:DelegateControl runat="server"
		ControlId="SmallSearchInputBox"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageDescription" runat="server">
	<SharePoint:ProjectProperty Property="Description" runat="server"/>
</asp:Content>

