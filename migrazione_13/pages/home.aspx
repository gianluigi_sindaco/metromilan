<%-- _lcid="1033" _version="15.0.4420" _dal="1" --%>
<%-- _LocalBinding --%>
<%@ Page language="C#" MasterPageFile="../_catalogs/masterpage/metromilan_13.master"    Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage,Microsoft.SharePoint,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" meta:webpartpageexpansion="full"  %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Import Namespace="Microsoft.SharePoint" %> <%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceHolderId="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePoint:ListItemProperty Property="BaseName" maxlength="40" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderSearchArea" runat="server">
	<SharePoint:DelegateControl runat="server"
		ControlId="SmallSearchInputBox"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageDescription" runat="server">
	<SharePoint:ProjectProperty Property="Description" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
	<div class="ms-hide">
	<WebPartPages:WebPartZone runat="server" title="loc:TitleBar" id="TitleBar" AllowLayoutChange="false" AllowPersonalization="false" Style="display:none;"><ZoneTemplate>
	<WebPartPages:TitleBarWebPart runat="server" HeaderTitle="Untitled_1" Title="Web Part Page Title Bar" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="TitleBar" PartOrder="2" FrameState="Normal" AllowRemove="False" AllowZoneChange="True" AllowMinimize="False" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_b0423985_e4de_4fa0_b112_c86a0d1746a1" AllowClose="False" ChromeType="None" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{B0423985-E4DE-4FA0-B112-C86A0D1746A1}" WebPart="true" Height="" Width=""></WebPartPages:TitleBarWebPart>

	</ZoneTemplate></WebPartPages:WebPartZone>
  </div>
  <table class="ms-core-tableNoSpace ms-webpartPage-root" width="100%">
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Header" ID="Header" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:LeftColumn" ID="LeftColumn" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:MiddleColumn" ID="MiddleColumn" FrameType="TitleBarOnly"><ZoneTemplate>
					<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{C3A9F835-18FA-420A-82A3-2C955096D1EE}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" FireInitialRow="False" Title="News" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="2" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_c3a9f835_18fa_420a_82a3_2c955096d1ee" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{C3A9F835-18FA-420A-82A3-2C955096D1EE}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.xsl</XslLink>
<XmlDefinition>
						<View Name="{C3A9F835-18FA-420A-82A3-2C955096D1EE}" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/home.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="Modified" Ascending="FALSE"/>
								</OrderBy>
								<Where>
									<And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto"/>
											<Value Type="Text">News</Value>
										</Eq>
										<Eq>
											<FieldRef Name="_ModerationStatus"/>
											<Value Type="ModStat">0</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="IconaCategoria"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="icona"/>
								<FieldRef Name="Author"/>
								<FieldRef Name="firma"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="ID"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="stile_box"/>
								<FieldRef Name="Title"/>
								<FieldRef Name="subtitle"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
								<Join Type="LEFT" ListAlias="firme">
									<Eq>
										<FieldRef Name="firma" RefType="Id"/>
										<FieldRef List="firme" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
								<Field Name="IconaCategoria" Type="Lookup" List="categorie" ShowField="Icona"/>
								<Field Name="icona" Type="Lookup" List="firme" ShowField="icona"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">6</RowLimit>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Standard"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{D4E5EE3F-4C08-4A3C-A653-A6E53E3EA1F7}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Schede Contenuto" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="4" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_d4e5ee3f_4c08_4a3c_a653_a6e53e3ea1f7" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{D4E5EE3F-4C08-4A3C-A653-A6E53E3EA1F7}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.xsl</XslLink>
<XmlDefinition>
						<View Name="{D4E5EE3F-4C08-4A3C-A653-A6E53E3EA1F7}" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/home.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="Modified" Ascending="FALSE"/>
								</OrderBy>
								<Where>
									<And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto"/>
											<Value Type="Text">Scheda Contenuto</Value>
										</Eq>
										<Eq>
											<FieldRef Name="_ModerationStatus"/>
											<Value Type="ModStat">0</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="IconaCategoria"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="icona"/>
								<FieldRef Name="Author"/>
								<FieldRef Name="firma"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="data_pubblicazione"/>
								<FieldRef Name="ID"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="stile_box"/>
								<FieldRef Name="subtitle"/>
								<FieldRef Name="Title"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
								<Join Type="LEFT" ListAlias="firme">
									<Eq>
										<FieldRef Name="firma" RefType="Id"/>
										<FieldRef List="firme" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
								<Field Name="IconaCategoria" Type="Lookup" List="categorie" ShowField="Icona"/>
								<Field Name="icona" Type="Lookup" List="firme" ShowField="icona"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">3</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="None"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{9F6CD806-A55F-44B6-9037-2ADEBF876FFD}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Item Elenco" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="6" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_9f6cd806_a55f_44b6_9037_2adebf876ffd" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{9F6CD806-A55F-44B6-9037-2ADEBF876FFD}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.xsl</XslLink>
<XmlDefinition>
						<View Name="{9F6CD806-A55F-44B6-9037-2ADEBF876FFD}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/home.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="Modified" Ascending="FALSE"/>
								</OrderBy>
								<Where>
									<And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto"/>
											<Value Type="Text">Item Elenco</Value>
										</Eq>
										<Eq>
											<FieldRef Name="_ModerationStatus"/>
											<Value Type="ModStat">0</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="IconaCategoria"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="icona"/>
								<FieldRef Name="Author"/>
								<FieldRef Name="firma"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="data_pubblicazione"/>
								<FieldRef Name="ID"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="stile_box"/>
								<FieldRef Name="subtitle"/>
								<FieldRef Name="Title"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
								<Join Type="LEFT" ListAlias="firme">
									<Eq>
										<FieldRef Name="firma" RefType="Id"/>
										<FieldRef List="firme" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
								<Field Name="IconaCategoria" Type="Lookup" List="categorie" ShowField="Icona"/>
								<Field Name="icona" Type="Lookup" List="firme" ShowField="icona"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">3</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="None"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

					</ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:RightColumn" ID="RightColumn" FrameType="TitleBarOnly"><ZoneTemplate>
<WebPartPages:ContentEditorWebPart runat="server" __MarkupType="xmlmarkup" WebPart="true" __WebPartId="{9A8FCEEC-9D71-4CD7-BE85-FF8A3AD48E59}" >
<WebPart xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.microsoft.com/WebPart/v2">
  <Title>Content Editor</Title>
  <FrameType>None</FrameType>
  <Description>Allows authors to enter rich text content.</Description>
  <IsIncluded>true</IsIncluded>
  <PartOrder>2</PartOrder>
  <FrameState>Normal</FrameState>
  <Height />
  <Width />
  <AllowRemove>true</AllowRemove>
  <AllowZoneChange>true</AllowZoneChange>
  <AllowMinimize>true</AllowMinimize>
  <AllowConnect>true</AllowConnect>
  <AllowEdit>true</AllowEdit>
  <AllowHide>true</AllowHide>
  <IsVisible>true</IsVisible>
  <DetailLink />
  <HelpLink />
  <HelpMode>Modeless</HelpMode>
  <Dir>Default</Dir>
  <PartImageSmall />
  <MissingAssembly>Cannot import this Web Part.</MissingAssembly>
  <PartImageLarge>/_layouts/15/images/mscontl.gif</PartImageLarge>
  <IsIncludedFilter />
  <ExportControlledProperties>true</ExportControlledProperties>
  <ConnectionID>00000000-0000-0000-0000-000000000000</ConnectionID>
  <ID>g_9a8fceec_9d71_4cd7_be85_ff8a3ad48e59</ID>
  <ContentLink xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor" />
  <Content xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor"><![CDATA[<div style="height: 5px;">&nbsp;</div>

<a class="no-decoration" href="/SitePages/all_news.aspx">
    <div class="category-container">
        <div class="category-row">
            <div class="category-cell">Tutte le News</div>

        </div>
    </div>
</a>
<a class="no-decoration" href="/SitePages/all_sc.aspx">
    <div class="category-container">
        <div class="category-row">
            <div class="category-cell">Tutte le Schede Contenuto</div>

        </div>
    </div>
</a>

<div class="category-container" id="rassegna-stampa-link" style="cursor: pointer;">
<!--    <div class="category-row" style="background-color: rgb(231, 46, 53);">
       <div class="category-cell">Rassegna Stampa</div>
    </div>
</div>-->


<a class="no-decoration" href="/SitePages/change_password.aspx">
    <div class="category-container">
        <div class="category-row">
            <div class="category-cell">Modifica Password</div>
        </div>
    </div>
</a>
​</div>
<!-- Anche questo è un link, ma è gestito tramite jQuery: -->
<!-- vedi rassegna-stampa.js -->

]]></Content>
  <PartStorage xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor" />
</WebPart>
</WebPartPages:ContentEditorWebPart>

<WebPartPages:ContentEditorWebPart runat="server" __MarkupType="xmlmarkup" WebPart="true" __WebPartId="{650289AA-6988-44D3-8EA5-CAD0872D9EB1}" >
<WebPart xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.microsoft.com/WebPart/v2">
  <Title>Content Editor</Title>
  <FrameType>None</FrameType>
  <Description>Allows authors to enter rich text content.</Description>
  <IsIncluded>true</IsIncluded>
  <PartOrder>4</PartOrder>
  <FrameState>Normal</FrameState>
  <Height />
  <Width />
  <AllowRemove>true</AllowRemove>
  <AllowZoneChange>true</AllowZoneChange>
  <AllowMinimize>true</AllowMinimize>
  <AllowConnect>true</AllowConnect>
  <AllowEdit>true</AllowEdit>
  <AllowHide>true</AllowHide>
  <IsVisible>true</IsVisible>
  <DetailLink />
  <HelpLink />
  <HelpMode>Modeless</HelpMode>
  <Dir>Default</Dir>
  <PartImageSmall />
  <MissingAssembly>Cannot import this Web Part.</MissingAssembly>
  <PartImageLarge>/_layouts/15/images/mscontl.gif</PartImageLarge>
  <IsIncludedFilter />
  <ExportControlledProperties>true</ExportControlledProperties>
  <ConnectionID>00000000-0000-0000-0000-000000000000</ConnectionID>
  <ID>g_650289aa_6988_44d3_8ea5_cad0872d9eb1</ID>
  <ContentLink xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor">/Web Contents/box_rassegna_stampa.html</ContentLink>
  <Content xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor" />
  <PartStorage xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor" />
</WebPart>
</WebPartPages:ContentEditorWebPart>

<WebPartPages:ContentEditorWebPart runat="server" __MarkupType="xmlmarkup" WebPart="true" __WebPartId="{CD121732-9B95-4CBE-BD8F-3AE8FF53A98E}" >
<WebPart xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.microsoft.com/WebPart/v2">
  <Title>Content Editor</Title>
  <FrameType>None</FrameType>
  <Description>Allows authors to enter rich text content.</Description>
  <IsIncluded>true</IsIncluded>
  <PartOrder>6</PartOrder>
  <FrameState>Normal</FrameState>
  <Height />
  <Width />
  <AllowRemove>true</AllowRemove>
  <AllowZoneChange>true</AllowZoneChange>
  <AllowMinimize>true</AllowMinimize>
  <AllowConnect>true</AllowConnect>
  <AllowEdit>true</AllowEdit>
  <AllowHide>true</AllowHide>
  <IsVisible>true</IsVisible>
  <DetailLink />
  <HelpLink />
  <HelpMode>Modeless</HelpMode>
  <Dir>Default</Dir>
  <PartImageSmall />
  <MissingAssembly>Cannot import this Web Part.</MissingAssembly>
  <PartImageLarge>/_layouts/15/images/mscontl.gif</PartImageLarge>
  <IsIncludedFilter />
  <ExportControlledProperties>true</ExportControlledProperties>
  <ConnectionID>00000000-0000-0000-0000-000000000000</ConnectionID>
  <ID>g_cd121732_9b95_4cbe_bd8f_3ae8ff53a98e</ID>
  <ContentLink xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor">/Web Contents/position-carousel.html</ContentLink>
  <Content xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor" />
  <PartStorage xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor" />
</WebPart>
</WebPartPages:ContentEditorWebPart>
<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{200D48F2-9070-4A75-B3F8-38B7554255F8}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{E16E0A26-E53A-453E-80A0-6FB14EE776E1}" ListId="e16e0a26-e53a-453e-80a0-6fb14ee776e1" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Scheda_Contenuto_Breve" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="8" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto_Breve" DetailLink="/Lists/Scheda_Contenuto_Breve" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_200d48f2_9070_4a75_b3f8_38b7554255f8" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{200D48F2-9070-4A75-B3F8-38B7554255F8}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.scb.xsl</XslLink>
<XmlDefinition>
						<View Name="{200D48F2-9070-4A75-B3F8-38B7554255F8}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/home.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="ID"/>
								</OrderBy>
								<Where>
									<And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto_breve"/>
											<Value Type="Text">Link</Value>
										</Eq>
										<Eq>
											<FieldRef Name="_ModerationStatus"/>
											<Value Type="ModStat">0</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="tipologia_scheda_contenuto_breve"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="link_interno"/>
								<FieldRef Name="link_esterno"/>
							</ViewFields>
							<RowLimit Paged="TRUE">20</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Standard"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

					</ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Footer" ID="Footer" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<SharePoint:ScriptBlock runat="server">
				if(typeof(MSOLayout_MakeInvisibleIfEmpty) == &quot;function&quot;) 
				
				
				
				{MSOLayout_MakeInvisibleIfEmpty();}</SharePoint:ScriptBlock>
		</table>
</asp:Content>
