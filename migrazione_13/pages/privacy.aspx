<%-- _lcid="1033" _version="15.0.4420" _dal="1" --%>
<%-- _LocalBinding --%>
<%@ Page language="C#" MasterPageFile="../_catalogs/masterpage/metromilan_13.master"    Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage,Microsoft.SharePoint,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" meta:webpartpageexpansion="full"  %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Import Namespace="Microsoft.SharePoint" %> <%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">

<div class="ms-hide">
	<WebPartPages:WebPartZone runat="server" title="loc:TitleBar" id="TitleBar" AllowLayoutChange="false" AllowPersonalization="false" Style="display:none;"><ZoneTemplate>
	<WebPartPages:TitleBarWebPart runat="server" HeaderTitle="Untitled_1" Title="Web Part Page Title Bar" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="TitleBar" PartOrder="2" FrameState="Normal" AllowRemove="False" AllowZoneChange="True" AllowMinimize="False" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_97653dfd_d397_46c0_9b65_de5db5273178" AllowClose="False" ChromeType="None" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{97653DFD-D397-46C0-9B65-DE5DB5273178}" WebPart="true" Height="" Width=""></WebPartPages:TitleBarWebPart>

	</ZoneTemplate></WebPartPages:WebPartZone>
  </div>
  <table class="ms-core-tableNoSpace ms-webpartPage-root" width="100%">
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Header" ID="Header" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:LeftColumn" ID="LeftColumn" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:MiddleColumn" ID="MiddleColumn" FrameType="TitleBarOnly"><ZoneTemplate>
<WebPartPages:ContentEditorWebPart runat="server" __MarkupType="xmlmarkup" WebPart="true" __WebPartId="{B8E75309-E596-44EE-A382-6CACF40CADD2}" >
<WebPart xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.microsoft.com/WebPart/v2">
  <Title>Content Editor</Title>
  <FrameType>None</FrameType>
  <Description>Allows authors to enter rich text content.</Description>
  <IsIncluded>true</IsIncluded>
  <PartOrder>2</PartOrder>
  <FrameState>Normal</FrameState>
  <Height />
  <Width />
  <AllowRemove>true</AllowRemove>
  <AllowZoneChange>true</AllowZoneChange>
  <AllowMinimize>true</AllowMinimize>
  <AllowConnect>true</AllowConnect>
  <AllowEdit>true</AllowEdit>
  <AllowHide>true</AllowHide>
  <IsVisible>true</IsVisible>
  <DetailLink />
  <HelpLink />
  <HelpMode>Modeless</HelpMode>
  <Dir>Default</Dir>
  <PartImageSmall />
  <MissingAssembly>Cannot import this Web Part.</MissingAssembly>
  <PartImageLarge>/_layouts/images/mscontl.gif</PartImageLarge>
  <IsIncludedFilter />
  <ExportControlledProperties>true</ExportControlledProperties>
  <ConnectionID>00000000-0000-0000-0000-000000000000</ConnectionID>
  <ID>g_b8e75309_e596_44ee_a382_6cacf40cadd2</ID>
  <ContentLink xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor" />
  <Content xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor"><![CDATA[<h1>INFORMATIVA RESA AI DIPENDENTI DI MM, ME, NME PER IL TRATTAMENTO DEI LORO 
DATI PERSONALI SU INTRANET AZIENDALE</h1>
<p>Desideriamo informarLa che sulla Intranet Aziendale di Metropolitana Milanese 
S.p.A. sono pubblicate alcune fotografie che la riguardano.
</p>
<p>Il D. Lgs. 196 del 30 giugno 2003 e s.m.i. (“Codice in materia di protezione 
dei dati personali”) prevede la tutela delle persone rispetto al trattamento dei 
dati personali compreso l’utilizzo, la raccolta, la registrazione, la 
conservazione di immagini, imponendo un trattamento improntato ai principi di 
correttezza, liceità e trasparenza e di tutela della Sua riservatezza e dei Suoi 
diritti.
</p>
<p>La informiamo che la nostra Società intende procedere alla pubblicazione 
delle immagini informandoLa che, secondo la normativa indicata: 
</p>
<ol>
  <li>Le immagini verranno utilizzate da parte di Metropolitana Milanese S.p.A. 
	solo ai fini dell’inserimento nella “Intranet Aziendale” e verranno pertanto 
	diffuse. 
  </li>
  <li>Metropolitana Milanese S.p.A. conserverà copia delle immagini che La 
	riguardano.
  </li>
  <li>Il conferimento dell’autorizzazione all’utilizzo delle immagini è 
	facoltativo. L’eventuale rifiuto comporterà divieto per Metropolitana 
	Milanese S.p.A. di procedere alla pubblicazione delle immagini che La 
	riguardano.
  </li>
  <li>Il titolare del trattamento è: Metropolitana Milanese S.p.A. nella persona 
	del suo Legale Rappresentante pro tempore.
  </li>
  <li>Il Responsabile del trattamento è la Dott.ssa Emanuela Teatini in qualità 
	di Responsabile Organizzazione e Sviluppo di Metropolitana Milanese S.p.A. 
  </li>
  <li>In ogni momento potrà esercitare i Suoi diritti nei confronti del Titolare 
	del trattamento, ai sensi dell’art. 7 del D. Lgs. 196/2003, che per Sua 
	comodità riproduciamo integralmente di seguito.
  </li>
</ol>
<h1>Decreto Legislativo 30 giugno 2003, n. 196</h1>
<h2>Art. 7. Diritto di accesso ai dati personali ed altri diritti</h2>
 
  <ol>
    <li>L&#39;interessato ha diritto di ottenere la conferma dell&#39;esistenza o meno 
	di dati personali che lo riguardano, anche se non ancora registrati, e la 
	loro comunicazione in forma intelligibile.</li>
    <li>L&#39;interessato ha diritto di ottenere l&#39;indicazione:
    <ol type="a"><li>dell&#39;origine dei dati personali;</li>
    <li>delle finalità e modalità del trattamento;</li>
    <li>della logica applicata in caso di trattamento effettuato con l&#39;ausilio 
	di strumenti elettronici;</li>
    <li>degli estremi identificativi del titolare, dei responsabili e del 
	rappresentante designato ai sensi dell&#39;articolo 5, comma 2;</li>
    <li>dei soggetti o delle categorie di soggetti ai quali i dati personali 
	possono essere comunicati o che possono venirne a conoscenza in qualità di 
	rappresentante designato nel territorio dello Stato, di responsabili o 
	incaricati.</li>
    </ol>
    </li>
    <li>L&#39;interessato ha diritto di ottenere:
    <ol type="a"><li>l&#39;aggiornamento, la rettificazione ovvero, quando vi ha 
		interesse, l&#39;integrazione dei dati;</li>
    <li>la cancellazione, la trasformazione in forma anonima o il blocco dei 
	dati trattati in violazione di legge, compresi quelli di cui non è 
	necessaria la conservazione in relazione agli scopi per i quali i dati sono 
	stati raccolti o successivamente trattati;</li>
    <li>&nbsp;&nbsp;&nbsp; l&#39;attestazione che le operazioni di cui alle lettere a) e b) sono 
	state portate a conoscenza, anche per quanto riguarda il loro contenuto, di 
	coloro ai quali i dati sono stati comunicati o diffusi, eccettuato il caso 
	in cui tale adempimento si rivela impossibile o comporta un impiego di mezzi 
	manifestamente sproporzionato rispetto al diritto tutelato.</li>
    </ol>
    </li>
    <li>
    L&#39;interessato ha diritto di opporsi, in tutto o in parte:
    <ol type="a">
    <li>per motivi legittimi al trattamento dei dati personali che lo 
	riguardano, ancorché pertinenti allo scopo della raccolta;</li>
    <li>al trattamento di dati personali che lo riguardano a fini di invio di 
	materiale pubblicitario o di vendita diretta o per il compimento di ricerche 
	di mercato o di comunicazione commerciale.</li>
    </ol>
    </li>
    </ol>
  ]]></Content>
  <PartStorage xmlns="http://schemas.microsoft.com/WebPart/v2/ContentEditor" />
</WebPart>
</WebPartPages:ContentEditorWebPart>
					</ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:RightColumn" ID="RightColumn" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Footer" ID="Footer" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<SharePoint:ScriptBlock runat="server">
				if(typeof(MSOLayout_MakeInvisibleIfEmpty) == &quot;function&quot;) 
				{MSOLayout_MakeInvisibleIfEmpty();}</SharePoint:ScriptBlock>
		</table>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderSearchArea" runat="server">
	<SharePoint:DelegateControl runat="server"
		ControlId="SmallSearchInputBox"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageDescription" runat="server">
	<SharePoint:ProjectProperty Property="Description" runat="server"/>
</asp:Content>

