<%-- _lcid="1033" _version="15.0.4420" _dal="1" --%>
<%-- _LocalBinding --%>
<%@ Page language="C#" MasterPageFile="../../_catalogs/masterpage/metromilan_13.master"    Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage,Microsoft.SharePoint,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" meta:webpartpageexpansion="full"  %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Import Namespace="Microsoft.SharePoint" %> <%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">

<div class="ms-hide">
	<WebPartPages:WebPartZone runat="server" title="loc:TitleBar" id="TitleBar" AllowLayoutChange="false" AllowPersonalization="false" Style="display:none;"><ZoneTemplate>
	<WebPartPages:TitleBarWebPart runat="server" HeaderTitle="Untitled_1" Title="Web Part Page Title Bar" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="TitleBar" PartOrder="2" FrameState="Normal" AllowRemove="False" AllowZoneChange="True" AllowMinimize="False" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_a11c9b89_31d1_4764_accc_5d076f1c8c12" AllowClose="False" ChromeType="None" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{A11C9B89-31D1-4764-ACCC-5D076F1C8C12}" WebPart="true" Height="" Width=""></WebPartPages:TitleBarWebPart>

	</ZoneTemplate></WebPartPages:WebPartZone>
  </div>
  <table class="ms-core-tableNoSpace ms-webpartPage-root" width="100%">
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Header" ID="Header" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:LeftColumn" ID="LeftColumn" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:MiddleColumn" ID="MiddleColumn" FrameType="TitleBarOnly"><ZoneTemplate>
					<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{490AE54F-6A78-4F4E-BCED-720B1CC760AE}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{8E8B4DEF-EC42-4948-806A-711C840EB9F7}" ListId="8e8b4def-ec42-4948-806a-711c840eb9f7" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Rassegna_Stampa" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="2" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Rassegna_Stampa" DetailLink="/Lists/Rassegna_Stampa" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_490ae54f_6a78_4f4e_bced_720b1cc760ae" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{490AE54F-6A78-4F4E-BCED-720B1CC760AE}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
					</ParameterBindings>
<Xsl>
<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal" xmlns:o="urn:schemas-microsoft-com:office:office"> 
  <xsl:include href="/_layouts/xsl/main.xsl"/> 
  <xsl:include href="/_layouts/xsl/internal.xsl"/> 
  						<xsl:param name="AllRows" select="/dsQueryResponse/Rows/Row[$EntityName = '' or (position() &gt;= $FirstRow and position() &lt;= $LastRow)]"/>
  						<xsl:param name="dvt_apos">&apos;</xsl:param>
						<xsl:template name="dvt_headerfield.Body" ddwrt:dvt_mode="header" ddwrt:ghost="" xmlns:ddwrt2="urn:frontpage:internal">
    						<xsl:param name="fieldname" />
    						<xsl:param name="fieldtitle" />
    						<xsl:param name="displayname"  />
    						<xsl:param name="fieldtype" select="'0'"/>
    						<xsl:variable name="separator" select="' '" />
    						<xsl:variable name="connector" select="';'" />
    						<xsl:variable name="linkdir">
      							<xsl:choose>
        							<xsl:when test="$dvt_sortfield = $fieldname and ($dvt_sortdir = 'ascending' or $dvt_sortdir = 'ASC')">Desc</xsl:when>
        							<xsl:otherwise>Asc</xsl:otherwise>
      							</xsl:choose>
    </xsl:variable>
    						<xsl:variable name="jsescapeddisplayname">
      							<xsl:call-template name="fixQuotes">
        							<xsl:with-param name="string" select="$displayname"/>
      							</xsl:call-template>
    </xsl:variable>
    						<xsl:variable name="sortable">
      							<xsl:choose>
        							<xsl:when test="../../@BaseViewID='3' and ../../List/@TemplateType='106'">FALSE</xsl:when>
        							<xsl:otherwise>
          <xsl:value-of select="./@Sortable"/>
        </xsl:otherwise>
      							</xsl:choose>
    </xsl:variable>
    						<xsl:choose>
      							<xsl:when test="$MasterVersion=4 and not($NoAJAX)">
        							<xsl:if test="(not($sortable='FALSE') and not(@FieldType='MultiChoice')) or (not(@Filterable='FALSE') and not(@FieldType='Note') and not(@FieldType='URL'))">
        <div class="s4-ctx">
          <span>&#160;</span>
            <a onfocus="OnChildColumn(this.parentNode.parentNode); return false;" onclick="PopMenuFromChevron(event); return false;" href="javascript:;" title="{$open_menu}">
            </a>
          <span>&#160;</span>
        </div>
      </xsl:if>
      </xsl:when>
      							<xsl:otherwise>
        							<xsl:attribute name="style">padding:0 !important;border:0 !important;</xsl:attribute>
        <div style="width:100%;position:relative;left:0;top:0;margin:0;border:0">
          	<xsl:choose>
            	<xsl:when test="$NoAJAX">
              <table CtxNum="{$ViewCounter}" cellspacing="1" cellpadding="0" class="ms-unselectedtitle" name="{$fieldname}" DisplayName="{$displayname}" height="100%">
                <xsl:choose>
                  	<xsl:when test="$MasterVersion=4">
                    	<xsl:attribute name="style">width:100%;height:27px</xsl:attribute>
                  </xsl:when>
                  	<xsl:otherwise>
                    	<xsl:attribute name="style">width:100%</xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="$sortable='FALSE'">
                  	<xsl:attribute name="Sortable">FALSE</xsl:attribute>
                </xsl:if>
                <xsl:if test="@Filterable='FALSE'">
                  	<xsl:attribute name="Filterable">FALSE</xsl:attribute>
                </xsl:if>
                <xsl:if test="@FilterDisableMessage">
                  	<xsl:attribute name="FilterDisableMessage">
                    <xsl:value-of select="@FilterDisableMessage" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="not($sortable='FALSE') or not(@Filterable='FALSE')">
                  	<xsl:attribute name="onmouseover">
                    	<xsl:text disable-output-escaping="yes">OnMouseOverAdHocFilter(this, &apos;</xsl:text>
                    <xsl:value-of select="concat($jsescapeddisplayname,$separator,$fieldname, $separator,$fieldtype, $connector, $LCID, $separator, $WebPartClientID)" />
                    	<xsl:text disable-output-escaping="yes">&apos; , &apos;&apos;, &apos;&apos;)</xsl:text>
                  </xsl:attribute>
                  	<xsl:attribute name="SortFields"><xsl:value-of select="concat($RootFolderParam,$FieldSortParam,'SortField=',@Name,'&amp;SortDir=',$linkdir)"/></xsl:attribute>
                  	<xsl:attribute name="FieldType"><xsl:value-of select="@FieldType"/></xsl:attribute>
                  	<xsl:attribute name="ResultType"><xsl:value-of select="@ResultType"/></xsl:attribute>
                </xsl:if>
                <xsl:call-template name="headerFieldRow">
                  	<xsl:with-param name="fieldname" select="$fieldname"/>
                  	<xsl:with-param name="fieldtitle" select ="$fieldtitle"/>
                  	<xsl:with-param name="displayname" select="$displayname"/>
                  	<xsl:with-param name="fieldtype" select ="$fieldtype"/>
                </xsl:call-template>
              </table>
            </xsl:when>
            	<xsl:otherwise>
              <table style="width:100%;" Sortable="{$sortable}" SortDisable="" FilterDisable="" Filterable="{@Filterable}" FilterDisableMessage="{@FilterDisableMessage}" name="{@Name}" CTXNum="{$ViewCounter}"
                 DisplayName="{@DisplayName}" FieldType="{@FieldType}" ResultType="{@ResultType}" SortFields="{$RootFolderParam}{$FieldSortParam}SortField={@Name}&amp;SortDir={$linkdir}"
                 height="100%" cellspacing="1" cellpadding="0" class="ms-unselectedtitle" onmouseover="OnMouseOverFilter(this)">
                <xsl:call-template name="headerFieldRow">
                  	<xsl:with-param name="fieldname" select="$fieldname"/>
                  	<xsl:with-param name="fieldtitle" select ="$fieldtitle"/>
                  	<xsl:with-param name="displayname" select="$displayname"/>
                  	<xsl:with-param name="fieldtype" select ="$fieldtype"/>
                </xsl:call-template>
              </table>
            </xsl:otherwise>
          	</xsl:choose>
        </div>
      </xsl:otherwise>
    						</xsl:choose>
  </xsl:template>
	
	</xsl:stylesheet></Xsl>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/rassegna/rassegna.detail.default.xsl</XslLink>
<XmlDefinition>
						<View Name="{490AE54F-6A78-4F4E-BCED-720B1CC760AE}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/rassegna/rassegna_stampa.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="ID" Ascending="FALSE"/>
								</OrderBy>
							</Query>
							<ViewFields>
								<FieldRef Name="ID"/>
								<FieldRef Name="Body"/>
								<FieldRef Name="LinkTitle"/>
							</ViewFields>
							<RowLimit Paged="TRUE">1</RowLimit>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Standard"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

					</ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:RightColumn" ID="RightColumn" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Footer" ID="Footer" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<SharePoint:ScriptBlock runat="server">if(typeof(MSOLayout_MakeInvisibleIfEmpty) == "function") {MSOLayout_MakeInvisibleIfEmpty();}</SharePoint:ScriptBlock>
		</table>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderSearchArea" runat="server">
	<SharePoint:DelegateControl runat="server"
		ControlId="SmallSearchInputBox"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageDescription" runat="server">
	<SharePoint:ProjectProperty Property="Description" runat="server"/>
</asp:Content>

