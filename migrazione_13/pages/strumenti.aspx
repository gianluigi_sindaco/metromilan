<%-- _lcid="1033" _version="15.0.4420" _dal="1" --%>
<%-- _LocalBinding --%>
<%@ Page language="C#" MasterPageFile="../_catalogs/masterpage/metromilan_13.master"    Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage,Microsoft.SharePoint,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" meta:webpartpageexpansion="full"  %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Import Namespace="Microsoft.SharePoint" %> <%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">

<div class="ms-hide">
	<WebPartPages:WebPartZone runat="server" title="loc:TitleBar" id="TitleBar" AllowLayoutChange="false" AllowPersonalization="false" Style="display:none;"><ZoneTemplate>
	<WebPartPages:TitleBarWebPart runat="server" HeaderTitle="Untitled_1" Title="Web Part Page Title Bar" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="TitleBar" PartOrder="2" FrameState="Normal" AllowRemove="False" AllowZoneChange="True" AllowMinimize="False" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_6162992f_dc38_49c6_aaa0_0133e9cdd97d" AllowClose="False" ChromeType="None" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{6162992F-DC38-49C6-AAA0-0133E9CDD97D}" WebPart="true" Height="" Width=""></WebPartPages:TitleBarWebPart>

	</ZoneTemplate></WebPartPages:WebPartZone>
  </div>
  <table class="ms-core-tableNoSpace ms-webpartPage-root" width="100%">
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Header" ID="Header" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:LeftColumn" ID="LeftColumn" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:MiddleColumn" ID="MiddleColumn" FrameType="TitleBarOnly"><ZoneTemplate>
					<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" NoDefaultStyle="" ViewGuid="{00120276-C5DF-496D-AE07-7411B7498615}" EnableOriginalValue="False" ViewContentTypeId="0x" ListUrl="" ListDisplayName="" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Scheda_Contenuto" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="2" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_00120276_c5df_496d_ae07_7411b7498615" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{00120276-C5DF-496D-AE07-7411B7498615}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
						<ParameterBinding Name="category" Location="QueryString(category)" DefaultValue="###" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.xsl</XslLink>
<XmlDefinition>
						<View Name="{00120276-C5DF-496D-AE07-7411B7498615}" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/strumenti.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="Modified" Ascending="FALSE"/>
								</OrderBy>
								<Where>
									<And>
										<And>
											<And>
												<Or>
													<Eq>
														<FieldRef Name="search_all"/>
														<Value Type="Text">{category}</Value>
													</Eq>
													<Eq>
														<FieldRef Name="IdCategoria"/>
														<Value Type="Text">{category}</Value>
													</Eq>
												</Or>
												<Eq>
													<FieldRef Name="_ModerationStatus"/>
													<Value Type="ModStat">0</Value>
												</Eq>
											</And>
											<Eq>
												<FieldRef Name="Area"/>
												<Value Type="Text">Strumenti</Value>
											</Eq>
										</And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto"/>
											<Value Type="Text">Scheda Contenuto</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="body"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="link_1"/>
								<FieldRef Name="link_2"/>
								<FieldRef Name="link_3"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="search_all"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="IconaCategoria"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="stile_box"/>
								<FieldRef Name="subtitle"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
								<Field Name="IconaCategoria" Type="Lookup" List="categorie" ShowField="Icona"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">1000</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{51246DCC-1957-46D0-948B-BF8BB8CD7900}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="News" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="4" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_51246dcc_1957_46d0_948b_bf8bb8cd7900" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{51246DCC-1957-46D0-948B-BF8BB8CD7900}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
						<ParameterBinding Name="category" Location="QueryString(category)" DefaultValue="###" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.xsl</XslLink>
<XmlDefinition>
						<View Name="{51246DCC-1957-46D0-948B-BF8BB8CD7900}" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/strumenti.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="Modified" Ascending="FALSE"/>
								</OrderBy>
								<Where>
									<And>
										<And>
											<And>
												<Or>
													<Eq>
														<FieldRef Name="search_all"/>
														<Value Type="Text">{category}</Value>
													</Eq>
													<Eq>
														<FieldRef Name="IdCategoria"/>
														<Value Type="Text">{category}</Value>
													</Eq>
												</Or>
												<Eq>
													<FieldRef Name="_ModerationStatus"/>
													<Value Type="ModStat">0</Value>
												</Eq>
											</And>
											<Eq>
												<FieldRef Name="Area"/>
												<Value Type="Text">Strumenti</Value>
											</Eq>
										</And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto"/>
											<Value Type="Text">News</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="body"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="link_1"/>
								<FieldRef Name="link_2"/>
								<FieldRef Name="link_3"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="search_all"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="IconaCategoria"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="stile_box"/>
								<FieldRef Name="subtitle"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
								<Field Name="IconaCategoria" Type="Lookup" List="categorie" ShowField="Icona"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">30</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{DCC468DF-0215-4225-85D7-7E9947AF9D62}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Scheda_Contenuto" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="6" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_dcc468df_0215_4225_85d7_7e9947af9d62" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{DCC468DF-0215-4225-85D7-7E9947AF9D62}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
						<ParameterBinding Name="category" Location="QueryString(category)" DefaultValue="###" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.item.elenco.sezione.xsl</XslLink>
<XmlDefinition>
						<View Name="{DCC468DF-0215-4225-85D7-7E9947AF9D62}" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/strumenti.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="Modified" Ascending="FALSE"/>
								</OrderBy>
								<Where>
									<And>
										<And>
											<And>
												<Or>
													<Eq>
														<FieldRef Name="search_all"/>
														<Value Type="Text">{category}</Value>
													</Eq>
													<Eq>
														<FieldRef Name="IdCategoria"/>
														<Value Type="Text">{category}</Value>
													</Eq>
												</Or>
												<Eq>
													<FieldRef Name="_ModerationStatus"/>
													<Value Type="ModStat">0</Value>
												</Eq>
											</And>
											<Eq>
												<FieldRef Name="Area"/>
												<Value Type="Text">Strumenti</Value>
											</Eq>
										</And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto"/>
											<Value Type="Text">Item Elenco</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="body"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="link_1"/>
								<FieldRef Name="link_2"/>
								<FieldRef Name="link_3"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="search_all"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="IconaCategoria"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="stile_box"/>
								<FieldRef Name="subtitle"/>
								<FieldRef Name="Title"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
								<Field Name="IconaCategoria" Type="Lookup" List="categorie" ShowField="Icona"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">100</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

					</ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:RightColumn" ID="RightColumn" FrameType="TitleBarOnly"><ZoneTemplate>
					<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" NoDefaultStyle="" ViewGuid="{B84E2222-E957-473E-AC07-335A390990DC}" EnableOriginalValue="False" ViewContentTypeId="0x" ListUrl="" ListDisplayName="" ListName="{40295D79-3C61-41EE-BB17-69495FE28FCA}" ListId="40295d79-3c61-41ee-bb17-69495fe28fca" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Categorie" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="2" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Categorie" DetailLink="/Lists/Categorie" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_b84e2222_e957_473e_ac07_335a390990dc" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{B84E2222-E957-473E-AC07-335A390990DC}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.categories.xsl</XslLink>
<XmlDefinition>
						<View Name="{B84E2222-E957-473E-AC07-335A390990DC}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/strumenti.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="ID"/>
								</OrderBy>
								<Where>
									<BeginsWith>
										<FieldRef Name="Codice"/>
										<Value Type="Text">ST</Value>
									</BeginsWith>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="Descrizione"/>
								<FieldRef Name="Codice"/>
							</ViewFields>
							<RowLimit Paged="TRUE">30</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

					</ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Footer" ID="Footer" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<SharePoint:ScriptBlock runat="server">
				if(typeof(MSOLayout_MakeInvisibleIfEmpty) == &quot;function&quot;) 
				
				{MSOLayout_MakeInvisibleIfEmpty();}</SharePoint:ScriptBlock>
		</table>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderSearchArea" runat="server">
	<SharePoint:DelegateControl runat="server"
		ControlId="SmallSearchInputBox"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageDescription" runat="server">
	<SharePoint:ProjectProperty Property="Description" runat="server"/>
</asp:Content>

