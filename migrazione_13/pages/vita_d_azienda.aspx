<%-- _lcid="1033" _version="15.0.4420" _dal="1" --%>
<%-- _LocalBinding --%>
<%@ Page language="C#" MasterPageFile="../_catalogs/masterpage/metromilan_13.master"    Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage,Microsoft.SharePoint,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" meta:webpartpageexpansion="full"  %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Import Namespace="Microsoft.SharePoint" %> <%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
<div class="ms-hide">
	<WebPartPages:WebPartZone runat="server" title="loc:TitleBar" id="TitleBar" AllowLayoutChange="false" AllowPersonalization="false" Style="display:none;"><ZoneTemplate>
	<WebPartPages:TitleBarWebPart runat="server" HeaderTitle="vita_d_azienda_13" Title="Web Part Page Title Bar" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" ZoneID="TitleBar" PartOrder="2" FrameState="Normal" AllowRemove="False" AllowZoneChange="True" AllowMinimize="False" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" DetailLink="" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="" IsIncludedFilter="" ExportControlledProperties="True" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_38a7b789_11f2_4e43_813e_ae27960c4087" AllowClose="False" ChromeType="None" ExportMode="All" __MarkupType="vsattributemarkup" __WebPartId="{38A7B789-11F2-4E43-813E-AE27960C4087}" WebPart="true" Height="" Width=""></WebPartPages:TitleBarWebPart>

	</ZoneTemplate></WebPartPages:WebPartZone>
  </div>
  <table class="ms-core-tableNoSpace ms-webpartPage-root" width="100%">
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Header" ID="Header" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:LeftColumn" ID="LeftColumn" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:MiddleColumn" ID="MiddleColumn" FrameType="TitleBarOnly"><ZoneTemplate>
					<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{A8459723-44B2-47CD-A7BF-EF789F4BEB84}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Scheda_Contenuto" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="2" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_a8459723_44b2_47cd_a7bf_ef789f4beb84" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{A8459723-44B2-47CD-A7BF-EF789F4BEB84}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
						<ParameterBinding Name="category" Location="QueryString(category)" DefaultValue="###" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.xsl</XslLink>
<XmlDefinition>
						<View Name="{A8459723-44B2-47CD-A7BF-EF789F4BEB84}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/vita_d_azienda.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="Modified" Ascending="FALSE"/>
								</OrderBy>
								<Where>
									<And>
										<And>
											<And>
												<Or>
													<Eq>
														<FieldRef Name="search_all"/>
														<Value Type="Text">{category}</Value>
													</Eq>
													<Eq>
														<FieldRef Name="IdCategoria"/>
														<Value Type="Text">{category}</Value>
													</Eq>
												</Or>
												<Eq>
													<FieldRef Name="_ModerationStatus"/>
													<Value Type="ModStat">0</Value>
												</Eq>
											</And>
											<Eq>
												<FieldRef Name="Area"/>
												<Value Type="Text">Vita d&#39;azienda</Value>
											</Eq>
										</And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto"/>
											<Value Type="Text">Scheda Contenuto</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="body"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="link_1"/>
								<FieldRef Name="link_2"/>
								<FieldRef Name="link_3"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="search_all"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="stile_box"/>
								<FieldRef Name="subtitle"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">30</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" ViewGuid="{8CDB5ED4-6612-4857-874F-3461A258F77D}" EnableOriginalValue="False" ViewContentTypeId="0x" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Scheda_Contenuto" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="4" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_8cdb5ed4_6612_4857_874f_3461a258f77d" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{8CDB5ED4-6612-4857-874F-3461A258F77D}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
						<ParameterBinding Name="category" Location="QueryString(category)" DefaultValue="###" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.xsl</XslLink>
<XmlDefinition>
						<View Name="{8CDB5ED4-6612-4857-874F-3461A258F77D}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/vita_d_azienda.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="Modified" Ascending="FALSE"/>
								</OrderBy>
								<Where>
									<And>
										<And>
											<And>
												<Or>
													<Eq>
														<FieldRef Name="search_all"/>
														<Value Type="Text">{category}</Value>
													</Eq>
													<Eq>
														<FieldRef Name="IdCategoria"/>
														<Value Type="Text">{category}</Value>
													</Eq>
												</Or>
												<Eq>
													<FieldRef Name="_ModerationStatus"/>
													<Value Type="ModStat">0</Value>
												</Eq>
											</And>
											<Eq>
												<FieldRef Name="Area"/>
												<Value Type="Text">Vita d&#39;azienda</Value>
											</Eq>
										</And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto"/>
											<Value Type="Text">News</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="body"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="link_1"/>
								<FieldRef Name="link_2"/>
								<FieldRef Name="link_3"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="search_all"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="stile_box"/>
								<FieldRef Name="subtitle"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">30</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" NoDefaultStyle="" ViewGuid="{F01FE0C0-B975-45CE-91F8-FA44C9B16CC6}" EnableOriginalValue="False" ViewContentTypeId="0x" ListUrl="" ListDisplayName="" ListName="{6E8DC81B-09DC-4DAB-AE02-EBC4A910D14D}" ListId="6e8dc81b-09dc-4dab-ae02-ebc4a910d14d" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Scheda_Contenuto" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="6" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Scheda_Contenuto" DetailLink="/Lists/Scheda_Contenuto" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_f01fe0c0_b975_45ce_91f8_fa44c9b16cc6" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{F01FE0C0-B975-45CE-91F8-FA44C9B16CC6}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
						<ParameterBinding Name="category" Location="QueryString(category)" DefaultValue="###" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.item.elenco.sezione.xsl</XslLink>
<XmlDefinition>
						<View Name="{F01FE0C0-B975-45CE-91F8-FA44C9B16CC6}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/vita_d_azienda.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<Where>
									<And>
										<And>
											<And>
												<Or>
													<Eq>
														<FieldRef Name="search_all"/>
														<Value Type="Text">{category}</Value>
													</Eq>
													<Eq>
														<FieldRef Name="IdCategoria"/>
														<Value Type="Text">{category}</Value>
													</Eq>
												</Or>
												<Eq>
													<FieldRef Name="_ModerationStatus"/>
													<Value Type="ModStat">0</Value>
												</Eq>
											</And>
											<Eq>
												<FieldRef Name="Area"/>
												<Value Type="Text">Vita d&#39;azienda</Value>
											</Eq>
										</And>
										<Eq>
											<FieldRef Name="tipologia_scheda_contenuto"/>
											<Value Type="Text">Item Elenco</Value>
										</Eq>
									</And>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="abstract"/>
								<FieldRef Name="body"/>
								<FieldRef Name="immagine"/>
								<FieldRef Name="link_1"/>
								<FieldRef Name="link_2"/>
								<FieldRef Name="link_3"/>
								<FieldRef Name="tipologia_scheda_contenuto"/>
								<FieldRef Name="categoria_contenuto"/>
								<FieldRef Name="search_all"/>
								<FieldRef Name="_ModerationStatus"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="IdCategoria"/>
								<FieldRef Name="Modified"/>
								<FieldRef Name="raccolta_documenti"/>
								<FieldRef Name="raccolta_immagini"/>
								<FieldRef Name="stile_box"/>
								<FieldRef Name="subtitle"/>
								<FieldRef Name="Title"/>
							</ViewFields>
							<Joins>
								<Join Type="INNER" ListAlias="categorie">
									<Eq>
										<FieldRef Name="categoria_contenuto" RefType="Id"/>
										<FieldRef List="categorie" Name="ID"/>
									</Eq>
								</Join>
							</Joins>
							<ProjectedFields>
								<Field Name="Area" Type="Lookup" List="categorie" ShowField="Area"/>
								<Field Name="IdCategoria" Type="Lookup" List="categorie" ShowField="ID"/>
							</ProjectedFields>
							<RowLimit Paged="TRUE">30</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

					</ZoneTemplate></WebPartPages:WebPartZone> </td>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" valign="top" height="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:RightColumn" ID="RightColumn" FrameType="TitleBarOnly"><ZoneTemplate>
					<WebPartPages:XsltListViewWebPart runat="server" ViewFlag="" ViewSelectorFetchAsync="False" InplaceSearchEnabled="False" ServerRender="False" ClientRender="False" InitialAsyncDataFetch="False" WebId="00000000-0000-0000-0000-000000000000" IsClientRender="False" GhostedXslLink="main.xsl" NoDefaultStyle="" ViewGuid="{B9035A57-4D4D-445E-9353-D92787D0884C}" EnableOriginalValue="False" ViewContentTypeId="0x" ListUrl="" ListDisplayName="" ListName="{40295D79-3C61-41EE-BB17-69495FE28FCA}" ListId="40295d79-3c61-41ee-bb17-69495fe28fca" PageSize="-1" UseSQLDataSourcePaging="True" DataSourceID="" ShowWithSampleData="False" AsyncRefresh="False" ManualRefresh="False" AutoRefresh="False" AutoRefreshInterval="60" Title="Categorie" FrameType="None" SuppressWebPartChrome="False" Description="" IsIncluded="True" PartOrder="2" FrameState="Normal" AllowRemove="True" AllowZoneChange="True" AllowMinimize="True" AllowConnect="True" AllowEdit="True" AllowHide="True" IsVisible="True" CatalogIconImageUrl="/_layouts/images/itgen.png" TitleUrl="/Lists/Categorie" DetailLink="/Lists/Categorie" HelpLink="" HelpMode="Modeless" Dir="Default" PartImageSmall="" MissingAssembly="Cannot import this Web Part." PartImageLarge="/_layouts/images/itgen.png" IsIncludedFilter="" ExportControlledProperties="False" ConnectionID="00000000-0000-0000-0000-000000000000" ID="g_b9035a57_4d4d_445e_9353_d92787d0884c" ChromeType="None" ExportMode="NonSensitiveData" __MarkupType="vsattributemarkup" __WebPartId="{B9035A57-4D4D-445E-9353-D92787D0884C}" __AllowXSLTEditing="true" __designer:CustomXsl="fldtypes_Ratings.xsl" WebPart="true" Height="" Width=""><ParameterBindings>
						<ParameterBinding Name="dvt_sortdir" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_sortfield" Location="Postback;Connection" />
						<ParameterBinding Name="dvt_startposition" Location="Postback" DefaultValue="" />
						<ParameterBinding Name="dvt_firstrow" Location="Postback;Connection" />
						<ParameterBinding Name="OpenMenuKeyAccessible" Location="Resource(wss,OpenMenuKeyAccessible)" />
						<ParameterBinding Name="open_menu" Location="Resource(wss,open_menu)" />
						<ParameterBinding Name="select_deselect_all" Location="Resource(wss,select_deselect_all)" />
						<ParameterBinding Name="idPresEnabled" Location="Resource(wss,idPresEnabled)" />
						<ParameterBinding Name="NoAnnouncements" Location="Resource(wss,noXinviewofY_LIST)" />
						<ParameterBinding Name="NoAnnouncementsHowTo" Location="Resource(wss,noXinviewofY_DEFAULT)" />
					</ParameterBindings>
<DataFields>
</DataFields>
<XslLink>
/_layouts/xsl/intranet/mm.categories.xsl</XslLink>
<XmlDefinition>
						<View Name="{B9035A57-4D4D-445E-9353-D92787D0884C}" MobileView="TRUE" Type="HTML" Hidden="TRUE" DisplayName="" Url="/SitePages/vita_d_azienda.aspx" Level="1" BaseViewID="1" ContentTypeID="0x" ImageUrl="/_layouts/15/images/generic.png?rev=23" >
							<Query>
								<OrderBy>
									<FieldRef Name="ID"/>
								</OrderBy>
								<Where>
									<BeginsWith>
										<FieldRef Name="Codice"/>
										<Value Type="Text">VA</Value>
									</BeginsWith>
								</Where>
							</Query>
							<ViewFields>
								<FieldRef Name="Attachments"/>
								<FieldRef Name="LinkTitle"/>
								<FieldRef Name="Area"/>
								<FieldRef Name="Descrizione"/>
								<FieldRef Name="Codice"/>
							</ViewFields>
							<RowLimit Paged="TRUE">30</RowLimit>
							<Aggregations Value="Off"/>
							<JSLink>clienttemplates.js</JSLink>
							<XslLink Default="TRUE">main.xsl</XslLink>
							<Toolbar Type="Freeform"/>
						</View>
					</XmlDefinition>
</WebPartPages:XsltListViewWebPart>

					</ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<tr>
					<td id="_invisibleIfEmpty" name="_invisibleIfEmpty" colspan="3" valign="top" width="100%"> 
					<WebPartPages:WebPartZone runat="server" Title="loc:Footer" ID="Footer" FrameType="TitleBarOnly"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> </td>
				</tr>
				<SharePoint:ScriptBlock runat="server">
				if(typeof(MSOLayout_MakeInvisibleIfEmpty) == &quot;function&quot;) 
				
				{MSOLayout_MakeInvisibleIfEmpty();}</SharePoint:ScriptBlock>
		</table>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderSearchArea" runat="server">
	<SharePoint:DelegateControl runat="server"
		ControlId="SmallSearchInputBox"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageDescription" runat="server">
	<SharePoint:ProjectProperty Property="Description" runat="server"/>
</asp:Content>

