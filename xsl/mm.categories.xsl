<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">

  <xsl:import href="/_layouts/xsl/mm.common.xsl"/>

  <xsl:output method="html" indent="no"/>
  <xsl:decimal-format NaN=""/>
  
  <xsl:variable name="category-id-prefix">mm-category-</xsl:variable>
  
  <xsl:variable name="mm_automode">0</xsl:variable>
  
  <!-- Parameters Binded - BEGIN-->

  <!-- Parameters Binded - END-->

  <xsl:template match="/" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:SharePoint="Microsoft.SharePoint.WebControls">
	<xsl:call-template name="mm"/>
  </xsl:template>

  <xsl:template name="mm">
    
    <xsl:variable name="Rows" select="/dsQueryResponse/Rows/Row"/>
    <xsl:variable name="mm_RowCount" select="count($Rows)"/>
    <xsl:variable name="mm_IsEmpty" select="$mm_RowCount = 0"/>

   
    <xsl:choose>
      <xsl:when test="$mm_IsEmpty">
        <xsl:call-template name="mm.empty"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mm.body">
			<xsl:with-param name="Rows" select="$Rows" />
		</xsl:call-template>
		<xsl:call-template name="mm.category.hover"/>
		
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>
  
  <xsl:template name="mm.body">
    <xsl:param name="Rows"/>      
    <div class="category-container">  
		<xsl:for-each select="$Rows">
		  <xsl:call-template name="mm.main" />
		</xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template name="mm.main">
    <!-- restituisce la classe del colore dell'area cui appartiene il contenuto -->
	
	<xsl:variable name="color-code">
      <xsl:choose>
        <xsl:when test="contains(@Area, 'azienda')">azure</xsl:when>
        <xsl:when test="contains(@Area, 'mestiere')">red</xsl:when>
        <xsl:when test="contains(@Area, 'Strumenti')">gray</xsl:when>
      </xsl:choose>
    </xsl:variable>
	
    <div class="category-row {@Area}" onmouseover="onCategoryOver({@ID}, '{$color-code}')" id="{$category-id-prefix}{@ID}" onmouseout="onCategoryOut({@ID}, '{$color-code}')">
      <div class="category-cell">
        <a href="?category={@ID}">
          <xsl:value-of select="@Title"/>
        </a>
      </div>
    </div>
  </xsl:template>
  
  <xsl:template name="mm.empty">
    <xsl:variable name="mm_ViewEmptyText">There are no items to show in this view.</xsl:variable>
        
    <table border="0" width="100%">
      <tr>
        <td class="ms-vb">
          <xsl:value-of select="$mm_ViewEmptyText"/>
        </td>
      </tr>
    </table>
  </xsl:template>
  
  <xsl:template name="mm.category.hover">
	<script type="text/javascript">
	
		var selectedCategoryId = -1;
		
		jQuery(document).ready(function(){
			var regex = /category=/g;
			
			//viene eseguito solo se � presente una query string e se in query string � presente il parametro 'category'
			if(regex.test(location.search)){
				var startParamValueIndex = regex.lastIndex;
				
				var endParamValueIndex = location.search.substring(startParamValueIndex).indexOf('&amp;') > -1 ? location.search.substring(startParamValueIndex).indexOf('&amp;') :
																												 location.search.substring(startParamValueIndex).length;
				selectedCategoryId = location.search.substring(startParamValueIndex, startParamValueIndex + endParamValueIndex);
				
				jQuery('#<xsl:value-of select="$category-id-prefix"/>' + selectedCategoryId).addClass(findAreaColor(selectedCategoryId));
			}
		});
	
		function onCategoryOver(index, areaColor){
			jQuery('#<xsl:value-of select="$category-id-prefix"/>' + index).addClass(areaColor);
		}
		
		function onCategoryOut(index, areaColor){
			if(index != selectedCategoryId){
				jQuery('#<xsl:value-of select="$category-id-prefix"/>' + index).removeClass(areaColor);
			}		
		}
		
		function findAreaColor(categoryId){
			
			var areaColor = '';
			
			va_regex = /Vita/g;
			st_regex = /Strumenti/g;
			inm_regex = /mestiere/g;
			
			var elem = jQuery('#<xsl:value-of select="$category-id-prefix"/>' + categoryId);

			if(va_regex.test(jQuery('#<xsl:value-of select="$category-id-prefix"/>' + categoryId).attr('class'))){
				areaColor = 'azure';
			}
			else if(st_regex.test(jQuery('#<xsl:value-of select="$category-id-prefix"/>' + categoryId).attr('class'))){
				
				areaColor = 'gray';
			}
			else if(inm_regex.test(jQuery('#<xsl:value-of select="$category-id-prefix"/>' + categoryId).attr('class'))){
				
				areaColor = 'red';
			}
			return areaColor;
		}

	</script>
  </xsl:template>
  
  
</xsl:stylesheet>
