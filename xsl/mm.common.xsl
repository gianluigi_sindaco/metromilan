<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
  <!-- Template e variabili comuni -->


  <!-- definiscono i codici colore da utilizzare-->
  <xsl:variable name="va-color">#A6B2CA</xsl:variable>
  <xsl:variable name="inm-color">#E72E35</xsl:variable>
  <xsl:variable name="st-color">#E9E9E9</xsl:variable>

  <!-- definiscono le classi css per impostare il colore di un box-->
  <xsl:variable name="va-color-class">bg-box-azure-color</xsl:variable>
  <xsl:variable name="inm-color-class">bg-box-red-color</xsl:variable>
  <xsl:variable name="st-color-class">bg-box-gray-color</xsl:variable>

</xsl:stylesheet>