<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
  <xsl:import href="/_layouts/xsl/main.xsl"/>

  <xsl:output method="html" indent="no"/>
  <xsl:decimal-format NaN=""/>

  <xsl:variable name="automode">0</xsl:variable>

  <!-- Parameters Binded - BEGIN-->
  <xsl:param name="section_title" />
  <xsl:param name="dvt_firstrow">1</xsl:param>
  <xsl:param name="nextpagedata" >1</xsl:param>
  <xsl:param name="apos">&apos;</xsl:param>
  <!-- Parameters Binded - END-->

  <xsl:template match="/" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:SharePoint="Microsoft.SharePoint.WebControls">
    <xsl:call-template name="mm"/>
  </xsl:template>

  <xsl:template name="mm">
    <xsl:variable name="Rows" select="/dsQueryResponse/Rows/Row"/>
    <xsl:variable name="RowCount" select="count($Rows)"/>
    <xsl:variable name="RowLimit" select="5" />
    <xsl:variable name="FirstRow" select="$dvt_firstrow" />
    <xsl:variable name="LastRow" select="$FirstRow + $RowLimit - 1" />
    <xsl:variable name="IsEmpty" select="$RowCount = 0"/>
    
    <xsl:choose>
      <xsl:when test="$IsEmpty">
        <xsl:call-template name="mm.empty"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mm.body">
          <xsl:with-param name="Rows" select="$Rows"/>
      	  <xsl:with-param name="FirstRow" select="$FirstRow" />
          <xsl:with-param name="LastRow" select="$LastRow" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:call-template name="mm.commandfooter">
      <xsl:with-param name="Rows" select="$Rows" />
      <xsl:with-param name="FirstRow" select="$FirstRow" />
      <xsl:with-param name="LastRow" select="$LastRow" />
      <xsl:with-param name="RowLimit" select="$RowLimit" />
      <xsl:with-param name="RowCount" select="$RowCount" />
      <xsl:with-param name="RealLastRow" select="number(ddwrt:NameChanged('',-100))" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="mm.body">
    <xsl:param name="Rows"/>
    <xsl:param name="FirstRow"/>
    <xsl:param name="LastRow"/>

    <!--<div style="border-left: 3px solid #A6B2C8; font-size: 14px; font-family: Arial Narrow; padding-left: 16px; color: #333; text-transform: uppercase; height: 16px; font-weight: bold; margin-bottom: 20px;">
      Documenti Utili
    </div>-->

    <table cellspacing="5" class="document-table">
      <thead style="text-align: left;">
        <th>Documento</th>
        <th>Modificato il</th>
      </thead>
      <xsl:for-each select="$Rows">
        <xsl:if test="(position() &gt;= $FirstRow and position() &lt;= $LastRow)">
          <tr>
          <td>
            <img src="/_layouts/images/{ddwrt:MapToIcon('',string(@File_x0020_Type))}" style="vertical-align:middle; margin-right: 10px;"></img>
            <a href="{@FileRef}" target="_blank">
              <xsl:value-of select="@FileLeafRef"/>
            </a>          
          </td>
          <td>
            <xsl:value-of select="@Modified"/>
          </td>
        </tr>
        </xsl:if>
        
      </xsl:for-each>    
    </table>

      
  </xsl:template> 

  <xsl:template name="mm.empty">
    <xsl:variable name="ViewEmptyText"></xsl:variable>
    <table border="0" width="100%">
      <tr>
        <td class="ms-vb">
          <xsl:value-of select="$ViewEmptyText"/>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="mm.commandfooter">

    <xsl:param name="Rows" />
    <xsl:param name="FirstRow" />
    <xsl:param name="LastRow" />
    <xsl:param name="RowLimit" />
    <xsl:param name="RowCount" />
    <xsl:param name="RealLastRow" />

    <table cellspacing="0" cellpadding="4" border="0" width="100%">
      <tr>
        <xsl:if test="$FirstRow &gt; 1 or $nextpagedata">
          <xsl:call-template name="mm.navigation">
            <xsl:with-param name="Rows" select="$Rows" />
            <xsl:with-param name="FirstRow" select="$FirstRow" />
            <xsl:with-param name="LastRow" select="$LastRow" />
            <xsl:with-param name="RowLimit" select="$RowLimit" />
            <xsl:with-param name="RowCount" select="$RowCount" />
            <xsl:with-param name="RealLastRow" select="$RealLastRow" />
          </xsl:call-template>
        </xsl:if>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="mm.navigation">
    <xsl:param name="Rows" />
    <xsl:param name="FirstRow" />
    <xsl:param name="LastRow" />
    <xsl:param name="RowLimit" />
    <xsl:param name="RowCount" />
    <xsl:param name="RealLastRow" />
    <xsl:variable name="PrevRow">
      <xsl:choose>
        <xsl:when test="$FirstRow - $RowLimit &lt; 1">1</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$FirstRow - $RowLimit" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="LastRowValue">
      <xsl:choose>
        <xsl:when test="$LastRow &gt; $RealLastRow">
          <xsl:value-of select="$LastRow"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$RealLastRow"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="NextRow">
      <xsl:value-of select="$LastRowValue + 1"></xsl:value-of>
    </xsl:variable>
    <td nowrap="nowrap" class="ms-paging" align="left">
      <xsl:variable name="NumPages" select="ceiling($RowCount div $RowLimit)"/>
      <xsl:if test="$NumPages &gt; 1">
        Pag.
        <xsl:text xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" ddwrt:nbsp-preserve="yes" disable-output-escaping="yes">&amp;nbsp;</xsl:text>

        <xsl:for-each select="$Rows">
          <xsl:if test="(position() mod $RowLimit) = 1">
            <xsl:choose>
              <xsl:when test="position() = $FirstRow">
                <xsl:value-of select="((position() - 1) div $RowLimit) + 1"/>
              </xsl:when>
              <xsl:otherwise>
                <a>
                  <xsl:attribute name="href">
                    javascript: <xsl:value-of xmlns:xsl="http://www.w3.org/1999/XSL/Transform" select="ddwrt:GenFireServerEvent(concat('dvt_firstrow={',position(),'}'))"/>;
                  </xsl:attribute>
                  <xsl:value-of select="((position() - 1) div $RowLimit) + 1"/>
                </a>
              </xsl:otherwise>
            </xsl:choose>

            <xsl:text xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" ddwrt:nbsp-preserve="yes" disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </xsl:if>
        </xsl:for-each>

      </xsl:if>
    </td>
  </xsl:template>
</xsl:stylesheet>
