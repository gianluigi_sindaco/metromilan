<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt"
                xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20"
                xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal" ddwrt:oob="true">

  <xsl:import href="/_layouts/xsl/main.xsl"/>
  <xsl:import href="/_layouts/xsl/mm.common.xsl"/>

  <xsl:output method="html" indent="no"/>
  <xsl:decimal-format NaN=""/>

  <xsl:variable name="mm_automode">0</xsl:variable>
  <xsl:variable name="abstract-max-chars" select="150" />

  <xsl:template match="/">
    <xsl:call-template name="mm" />
  </xsl:template>

  <xsl:template name="mm">

	  <xsl:variable name="Rows" select="/dsQueryResponse/Rows/Row"/>
	  <xsl:variable name="RowCount" select="count($Rows)"/>
      <xsl:variable name="RowLimit">
	  <xsl:variable name="FirstRow" select="$dvt_firstrow" />
      <xsl:variable name="LastRow" select="$FirstRow + $RowLimit - 1" />
      <xsl:variable name="IsEmpty" select="$RowCount = 0"/>
	  
      <xsl:variable name="section_title">
		
      </xsl:variable>

    <xsl:choose>
      <xsl:when test="$IsEmpty">

      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mm.body">
          <xsl:with-param name="Rows" select="$Rows"/>
          <xsl:with-param name="FirstRow" select="$FirstRow" />
          <xsl:with-param name="LastRow" select="$LastRow" />
          <xsl:with-param name="RowLimit" select="$RowLimit" />
          <xsl:with-param name="RowCount" select="$RowCount" />
          <xsl:with-param name="tipologia_scheda_contenuto" select="$tipologia_scheda_contenuto" />
		  <xsl:with-param name="section_title" select="$section_title" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

<xsl:template name="mm.body">
    <xsl:param name="Rows"/>
    <xsl:param name="FirstRow" />
    <xsl:param name="LastRow" />
    <xsl:param name="RowLimit" />
    <xsl:param name="RowCount" />
    <xsl:param name="tipologia_scheda_contenuto" />
    <xsl:param name="section_title" />  
    
    <xsl:variable name="NumPages" select="ceiling($RowCount div $RowLimit)"/>
    
    <xsl:variable name="Num_Items_For_Row">3</xsl:variable>
       
    <div style="width: 700px;">
      <!--<div class="section-title">
        <xsl:value-of select="$section_title"/>
      </div>-->
      <xsl:for-each select="$Rows">
          <xsl:if test="(position() &gt;= $FirstRow and position() &lt;= $LastRow)">
            <xsl:call-template name="mm.itemview" />
          </xsl:if>

          <xsl:if test = "(position() &gt;= $FirstRow and position() &lt;= $LastRow and (position() mod $Num_Items_For_Row = 0))">
            <div style="clear: both; width: 100%; height: 1px;" />
          </xsl:if>
        </xsl:for-each>
    </div>
  </xsl:template>

  
  <xsl:template name="mm.itemview">
    <!-- definisce l'url completo alla pagina di dettaglio (url + querystring)-->
    <xsl:variable name="complete-detail-url-and-querystring">
      <xsl:choose>
        <xsl:when test="contains(@Area,'azienda')">vita_d_azienda.aspx?category=<xsl:value-of select="@IdCategoria"/></xsl:when>
        <xsl:when test="contains(@Area,'Strumenti')">strumenti.aspx?category=<xsl:value-of select="@IdCategoria"/></xsl:when>
        <xsl:when test="contains(@Area,'mestiere')">il_nostro_mestiere.aspx?category=<xsl:value-of select="@IdCategoria"/></xsl:when>
      </xsl:choose>
    </xsl:variable>

    <!-- restituisce la classe per il colore dell'area cui appartiene il contenuto -->
    <xsl:variable name="color-class">
      <xsl:choose>
        <xsl:when test="contains(@Area, 'azienda')">
          <xsl:value-of select="$va-color-class"/>
        </xsl:when>
        <xsl:when test="contains(@Area, 'mestiere')">
          <xsl:value-of select="$inm-color-class"/>
        </xsl:when>
        <xsl:when test="contains(@Area, 'Strumenti')">
          <xsl:value-of select="$st-color-class"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>


 <div class="row-sc-item {$color-class}">
          <div>
            <a href="{$complete-detail-url-and-querystring}">
              <xsl:choose>
                <xsl:when test="@stile_box = 'background immagine'">
                  <xsl:call-template name="mm.sc.background.image" />
                </xsl:when>
                <xsl:when test="@stile_box = 'miniatura immagine'">
                  <xsl:call-template name="mm.sc.limited.image" />
                </xsl:when>
                <xsl:when test="@stile_box = 'solo testo'">
                  <xsl:call-template name="mm.sc.no.image" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:call-template name="mm.sc.no.image" />
                </xsl:otherwise>
              </xsl:choose>
            </a>
          </div>
        </div>
  </xsl:template>

  <!-- ************************************************-->
  <!-- ************************************************-->
  <!-- **************** SCHEDE CONTENUTO **************-->
  <!-- ************************************************-->
  <!-- ************************************************-->

  <xsl:template name="mm.sc.background.image">

    <!-- lunghezza in caratteri dell'abstract -->
    <xsl:variable name="abstract-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">50</xsl:when>
        <xsl:otherwise>40</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <div class="sc-box-sc">

      <div class="title">
        <xsl:call-template name="cutText">
          <xsl:with-param name="text" select="@Title" />
          <xsl:with-param name="limit" select="20" />
        </xsl:call-template>
      </div>

      <xsl:if test="@immagine != ''" >
        <xsl:variable name="img-src">
          <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
        </xsl:variable>
        <div style="width:100%; height: 65px; background:url({$img-src}) no-repeat center center; margin-bottom: 5px;">
        </div>
      </xsl:if>

      <div class="abstract">
        <xsl:call-template name="cutText">
          <xsl:with-param name="text" select="@abstract" />
          <xsl:with-param name="limit" select="$abstract-length" />
        </xsl:call-template>
      </div>

      <div class="date">
        <xsl:call-template name="correctDate">
          <xsl:with-param name="date" select="@Modified" />
          <xsl:with-param name="format" select="'dd MMMM yyyy'" />
        </xsl:call-template>
      </div>

    </div>
  </xsl:template>

  <xsl:template name="mm.sc.limited.image">
    <div class="sc-box-sc">
      
      <div class="title">
        <xsl:call-template name="cutText">
          <xsl:with-param name="text" select="@Title" />
          <xsl:with-param name="limit" select="30" />
        </xsl:call-template>
      </div>

      <xsl:if test="@immagine != ''" >
        <xsl:variable name="img-src">
          <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
        </xsl:variable>
        <img src="{$img-src}" style="float: left; margin-right: 10px; max-width: 80px; max-height: 100px;"/>
      </xsl:if>

      <p class="abstract">
        <xsl:choose>
          <xsl:when test="@immagine != ''">
            <xsl:value-of select="concat(substring(@abstract, 1, 50), '...')" disable-output-escaping="yes"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="string-length(@abstract) &gt; $abstract-max-chars">
                <xsl:value-of select="concat(substring(@abstract, 1, $abstract-max-chars), '...')" disable-output-escaping="yes"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@abstract" disable-output-escaping="yes"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </p>

      <div class="date">
        <xsl:call-template name="correctDate">
          <xsl:with-param name="date" select="@Modified" />
          <xsl:with-param name="format" select="'dd MMMM yyyy'" />
        </xsl:call-template>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="mm.sc.no.image">

    <div class="sc-box-sc">
       
      <div class="title">
        <xsl:call-template name="cutText">
          <xsl:with-param name="text" select="@Title" />
          <xsl:with-param name="limit" select="20" />
        </xsl:call-template>
      </div>

      <p class="abstract">
        <xsl:choose>
          <xsl:when test="@immagine != ''">
            <xsl:value-of select="concat(substring(@abstract, 1, 50), '...')" disable-output-escaping="yes"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="string-length(@abstract) &gt; $abstract-max-chars">
                <xsl:value-of select="concat(substring(@abstract, 1, $abstract-max-chars), '...')" disable-output-escaping="yes"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@abstract" disable-output-escaping="yes"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </p>

      <div class="date">
        <xsl:call-template name="correctDate">
          <xsl:with-param name="date" select="@Modified" />
          <xsl:with-param name="format" select="'dd MMMM yyyy'" />
        </xsl:call-template>
      </div>

      </div>
  </xsl:template>

  <!-- ************************************************-->
  <!-- ************************************************-->
  <!-- ********** COMMON TEMPLATES ********************-->
  <!-- ************************************************-->
  <!-- ************************************************-->

  <!-- corregge la formattazione della data-->
  <xsl:template name="correctDate">
    <xsl:param name="date" />
    <xsl:param name="format" />

    <xsl:variable name="a" select="substring(string($date),4,3)" />
    <xsl:variable name="b" select="substring(string($date),1,3)" />
    <xsl:variable name="c" select="substring(string($date),7,4)" />
    <xsl:variable name="correctedModifiedDate" select="concat($a,$b,$c)" />

    <xsl:value-of select="ddwrt:FormatDateTime($correctedModifiedDate, 1040,  $format)" />
  </xsl:template>

  <!-- taglia un testo se (e solo se) supera un certo numero di caratteri-->
  <xsl:template name="cutText">

    <xsl:param name="text" />
    <xsl:param name="limit" />

    <xsl:choose>
      <xsl:when test="string-length($text) &gt; $limit">
        <xsl:value-of select="concat(substring($text, 1, $limit), '...')" disable-output-escaping="yes"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" disable-output-escaping="yes"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
