<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
  
  <xsl:import href="/_layouts/xsl/main.xsl"/>
  <xsl:import href="/_layouts/xsl/mm.common.xsl"/>
  
  <xsl:output method="html" indent="no"/>
  <xsl:decimal-format NaN=""/>
  
  <xsl:variable name="mm_automode">0</xsl:variable>
  
  <!-- Parameters Binded - BEGIN-->
  <xsl:param name="section_title" />
  <xsl:param name="SC_ID" />
  <xsl:param name="UserID" />
  <!-- Il valore di ListID � il parametro List definito nel main.xsl-->
  <!-- Parameters Binded - END-->

  <xsl:template match="/">
	<!--<xmp>
		<xsl:copy-of select="*" />
	</xmp>-->
    <xsl:call-template name="mm" />
  </xsl:template>

  <xsl:template name="mm">
    <xsl:variable name="Rows" select="/dsQueryResponse/Rows/Row"/>
    <xsl:variable name="mm_RowCount" select="count($Rows)"/>
    <xsl:variable name="mm_IsEmpty" select="$mm_RowCount = 0"/>
    <xsl:variable name="tipologia_scheda_contenuto" select="$XmlDefinition/Query/Where//Value[../FieldRef[@Name='tipologia_scheda_contenuto']]"/>
   
    <xsl:choose>
      <xsl:when test="$mm_IsEmpty">
        <xsl:call-template name="mm.empty"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mm.body">
          <xsl:with-param name="Rows" select="$Rows"/>
          <xsl:with-param name="tipologia_scheda_contenuto" select="$tipologia_scheda_contenuto" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose> 
  </xsl:template>
  
  <xsl:template name="mm.body">
    <xsl:param name="Rows"/>
    <xsl:param name="tipologia_scheda_contenuto" />

    <div style="width: 700px; padding-right: 20px;">
      <div>
        <xsl:for-each select="$Rows">
          <xsl:call-template name="mm.itemview">
            <xsl:with-param name="tipologia_scheda_contenuto" select="$tipologia_scheda_contenuto" />
          </xsl:call-template>
        </xsl:for-each>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="mm.itemview">
    <xsl:param name="tipologia_scheda_contenuto" />
   
    
<!--<xsl:if test="@tipologia_scheda_contenuto='News' or @tipologia_scheda_contenuto='Scheda Contenuto' or @tipologia_scheda_contenuto='Item Elenco'">
	<xsl:call-template name="detail.breadcrumb">
		<xsl:with-param name="tipologia_scheda_contenuto" select="tipologia_scheda_contenuto" />
    </xsl:call-template>
</xsl:if>-->

      
    
    <xsl:choose>
      <xsl:when test="@tipologia_scheda_contenuto='News'">
        <xsl:call-template name="mm.news.detail" />
      </xsl:when>
      <xsl:when test="@tipologia_scheda_contenuto='Scheda Contenuto' or @tipologia_scheda_contenuto='Item Elenco'">
        <xsl:call-template name="mm.sc.detail" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mm.blog.detail" />
      </xsl:otherwise>
    </xsl:choose>
    
    <xsl:if test="@tipo_feedback != ''">
      <xsl:call-template name="ilike.template" />
    </xsl:if>

  </xsl:template>

  <!-- breadcrumb -->
  <xsl:template name="detail.breadcrumb">
    <xsl:param name="tipologia_scheda_contenuto" />

    <xsl:variable name="color-code">
      <xsl:choose>
        <xsl:when test="contains(@Area, 'azienda')">
          <xsl:value-of select="$va-color"/>
        </xsl:when>
        <xsl:when test="contains(@Area, 'mestiere')">
          <xsl:value-of select="$inm-color"/>
        </xsl:when>
        <xsl:when test="contains(@Area, 'Strumenti')">
          <xsl:value-of select="$st-color"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
	
    <div style="margin-bottom: 20px; height: 50px;">
      <div style="width: 50px; height: 50px; background-color: {$color-code}; float: left;" />
      <div style="width: 350px; height: 50px;  padding-left: 20px; line-height: 50px; float: left; ">
        <span style="color: #808080; font-size: 10 px; ">
          <xsl:choose>
            <xsl:when test="@tipologia_scheda_contenuto='News'">
              News :: <xsl:value-of select="@Area"/>
            </xsl:when>
            <xsl:when test="@tipologia_scheda_contenuto='Scheda Contenuto'">
              Scheda Contenuto :: <xsl:value-of select="@Area"/>
            </xsl:when>
            <xsl:otherwise>
              Blog :: <xsl:value-of select="@Area"/>
            </xsl:otherwise>
          </xsl:choose>
        </span>
      </div>      
    </div>
    <div style="clear:both;" />
  </xsl:template>

  <xsl:template name="mm.empty">
    <xsl:variable name="mm_ViewEmptyText">There are no items to show in this view.</xsl:variable>     
    <table border="0" width="100%">
      <tr>
        <td class="ms-vb">
          <xsl:value-of select="$mm_ViewEmptyText"/>
        </td>
      </tr>
    </table>
  </xsl:template>
  

  <!-- ************************************************-->
  <!-- ************************************************-->
  <!-- ******************* NEWS ***********************-->
  <!-- ************************************************-->
  <!-- ************************************************-->
  <xsl:template name="mm.news.detail">
  
    <xsl:variable name="color-code">
      <xsl:choose>
        <xsl:when test="contains(@Area, 'azienda')">
          <xsl:value-of select="$va-color"/>
        </xsl:when>
        <xsl:when test="contains(@Area, 'mestiere')">
          <xsl:value-of select="$inm-color"/>
        </xsl:when>
        <xsl:when test="contains(@Area, 'Strumenti')">
          <xsl:value-of select="$st-color"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
  
    <div class="content-detail">
      <div class="header">
            <div class="header-title">
			  
			  <div style="width: 50px; height: 50px; background-color: {$color-code}; float: left;" />
              <div style="float: left; width: 90%; margin-left: 10px;">
				  <div class="sc header-title-text">
					<xsl:value-of select="@Title"/>
				  </div>

				  <div class="header-pubblication-date">
					<xsl:variable name="pubblicato_il">Pubblicato il: </xsl:variable>
					<xsl:value-of select="concat($pubblicato_il, @Modified)"/>
				  </div>			  
			  </div>
            </div>          
      </div>
      <div style="clear: both;" />
      <div class="content-body">
	  	<!--<xsl:if test="@immagine != ''" >
        <xsl:variable name="img-src">
          <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
        </xsl:variable>
        <img src="{$img-src}" style="max-width: 50%; margin-top: 10px; float: left; margin: 0px 25px 25px 0px;"/>
      </xsl:if>-->
	  	
		<xsl:variable name="is-ods-comunicazione-procedura">
			<xsl:value-of select="@IdCategoria=12 or @IdCategoria=13 or @IdCategoria=15"/>			
		</xsl:variable>
		
		<xsl:variable name="img-src">
		  <xsl:choose>
			<xsl:when test="$is-ods-comunicazione-procedura='true'">
				<xsl:value-of select="@IconaCategoria"/>	
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="@immagine != ''" >
					<xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
				</xsl:if>				
			</xsl:otherwise>
		  </xsl:choose>
		</xsl:variable>

		<xsl:if test="$img-src != ''" >
			<xsl:choose>
				<xsl:when test="$is-ods-comunicazione-procedura='true'">
					<img src="{$img-src}" style="width: 150px; margin-top: 10px; float: left; margin: 0px 25px 25px 0px;"/>					
				</xsl:when>
				<xsl:otherwise>
					<img src="{$img-src}" style="max-width: 50%; margin-top: 10px; float: left; margin: 0px 25px 25px 0px;"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	  
      <p>
        <xsl:value-of select="@body" disable-output-escaping="yes"/>    
      </p>
      </div>
	<div style="clear:both"></div>  
	  <!--<div class="sign">
		<xsl:variable name="autore">
			<xsl:choose>
				<xsl:when test="@firma != ''">
					<xsl:value-of select="substring-after(@firma.,';#')" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@Editor.title" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
	  </div>-->
	<xsl:if test="@firma != ''">
		<div class="sign">
			<xsl:value-of select="substring-after(@firma.,';#')" />
		</div>
	</xsl:if>
	  
	  <div style="border-bottom: 1px dotted; height: 1px; width: 100%; margin: 15px 0 15px 0;" />

          <xsl:if test="@Attachments = 1">
           <div style="margin-top: 20px;">
            Allegati:
            <xsl:element name="SharePoint:AttachmentsField">
            <xsl:attribute name="runat">server</xsl:attribute>
            <xsl:attribute name="FieldName">Attachments</xsl:attribute>
            <xsl:attribute name="ControlMode">Display</xsl:attribute>
            <xsl:attribute name="Visible">true</xsl:attribute>
            <xsl:attribute name="ItemId">
             <xsl:value-of select="@ID"/>
            </xsl:attribute>
          </xsl:element>
         </div>
	<div style="border-bottom: 1px dotted; height: 1px; width: 100%; margin: 15px 0 15px 0;" />
        </xsl:if>

	  
	  <xsl:if test="@link_1 != '' or @link_2 != '' or @link_3 != ''">
		  <div style="margin-top: 20px;">
			Link utili:
		  </div>
		  <ul>
		  <xsl:if test="@link_1 != ''">
			<li>
				<a href="{@link_1}" target="_self"><xsl:value-of select="@link_1.desc" /></a>
			</li>
		  </xsl:if>
		  <xsl:if test="@link_2 != ''">
			<li>
				<a href="{@link_2}" target="_self"><xsl:value-of select="@link_2.desc" /></a>
			</li>			
		  </xsl:if>
		  <xsl:if test="@link_3 != ''">
			<li>
				<a href="{@link_3}" target="_self"><xsl:value-of select="@link_3.desc" /></a>
			</li>			
		  </xsl:if>
		  </ul>
	   <div style="border-bottom: 1px dotted; height: 1px; width: 100%; margin: 15px 0 15px 0;" />
	  </xsl:if>

    </div>  
  </xsl:template>

  <!-- ************************************************-->
  <!-- ************************************************-->
  <!-- *********** SCHEDA CONTENUTO *******************-->
  <!-- ************************************************-->
  <!-- ************************************************-->

  <xsl:template name="mm.sc.detail">
  <!-- Per ora richiamiamo lo stesso template di news -->
	<xsl:call-template name="mm.news.detail" />
  </xsl:template>

  <!-- ************************************************-->
  <!-- ************************************************-->
  <!-- ******************* BLOG ***********************-->
  <!-- ************************************************-->
  <!-- ************************************************-->

  <xsl:template name="mm.blog.detail">

    <xsl:variable name="firma">
      <xsl:choose>
        <xsl:when test="@Firma. != ''">
          <xsl:value-of select="substring-after(@Firma.,';#')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@Author.title"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="user-image">
      <xsl:choose>
        <xsl:when test="@Firma. != ''">
          <xsl:choose>
            <xsl:when test="@icona != ''">
              <xsl:value-of select="@icona"/>
            </xsl:when>
            <xsl:otherwise>generic-user.png</xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>generic-user.png</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <div class="content-detail">
      <div style="height: 100px;">
        <!--<div style="margin-top: 5px; display: table; margin-bottom: 10px; float: left;">
          <img src="/SiteCollectionImages/immagini-firme/{$user-image}" width="72px" height="72px" style="border: 3px solid #E9EAEA;"/>
        </div>-->
        <div class="header-title" style="height: 100px; font-family: Arial; font-size: 12px; color: #333333; height: 100px;">
          <div class="sc header-title-text" style="font-size: 16px; font-weight: bold; display:table-cell; vertical-align: middle; height: 85px;">
            <xsl:value-of select="@Title"/>
          </div>
        </div>
      </div>
      
      <!--<div style="font-size: 12px;">
        Autore: <span style="font-style: italic; font-weight: bold;"><xsl:value-of select="$firma"/></span>
      </div>-->
      
      <div style="font-size: 12px;">
        <xsl:variable name="pubblicato_il">Pubblicato il: </xsl:variable>
        <xsl:value-of select="concat($pubblicato_il, @Modified)"/>
      </div>

      <div class="content-body">
	  	  	<xsl:if test="@immagine != ''" >
        <xsl:variable name="img-src">
          <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
        </xsl:variable>
        <img src="{$img-src}" style="max-width: 100%; margin-top: 10px;"/>
      </xsl:if>
        <xsl:value-of select="@body" disable-output-escaping="yes"/>
      </div>

	  <xsl:if test="@firma != ''">
		<div class="sign">
			<xsl:value-of select="substring-after(@firma.,';#')" />
		</div>
	  </xsl:if>
	  
	  
      <div id="documents-placeholder">
        <ul id="document-placeholder-list">

        </ul>
      </div>

    </div>
  </xsl:template>

  <!-- ************************************************-->
  <!-- ************************************************-->
  <!-- ******************* ILIKE **********************-->
  <!-- ************************************************-->
  <!-- ************************************************-->
  
  <xsl:template name="ilike.template">
    <div class="i-like-section">
      <div id="feedback-report" style="font-size: 12px; font-weight: bold; line-height:35px; float: left;">

      </div>
      <a href="javascript:vote()">
        <div id="voteButton" style="color: white; background-color: green; height: 40px; width: 120px; text-align: center; line-height: 34px; font-size: 15px; float: left; margin-left: 25px; display: none;">Mi interessa</div>
      </a>
      <div class="modal">
       
      </div>
    </div>

    <script type="text/javascript">

      var currentSiteUrl = '/';
      var currentTargetWeb;
      var currentClientContext;
      var content_feedback_list;

      function initEnvironment(){
      currentClientContext = new SP.ClientContext(currentSiteUrl);
      content_feedback_list = currentClientContext.get_web().get_lists().getByTitle('Content_FeedBack');
      currentTargetWeb = currentClientContext.get_web();
      }

      //invoca la query per recuperare i risultati dei voti per l'articolo della lista corrente
      function retrieveILikeResults() {

      var view = '<View>
        <Query>
          <Where>
            <And>
              <Eq>
                <FieldRef Name="IdLista" />
                <Value Type="Text">
                  <xsl:value-of select="$List"/>
                </Value>
              </Eq>
              <Eq>
                <FieldRef Name="IdContenuto" />
                <Value Type="Text">
                  <xsl:value-of select="$SC_ID"/>
                </Value>
              </Eq>
            </And>
          </Where>
        </Query>
      </View>';

      var camlQuery = new SP.CamlQuery();

      camlQuery.set_viewXml(view);

      allItems = content_feedback_list.getItems(camlQuery);

      currentClientContext.load(allItems);
      
      currentClientContext.executeQueryAsync(Function.createDelegate(this, this.onResultsQuerySucceeded), Function.createDelegate(this, this.onResultsQueryFailed));

      jQuery(".i-like-section").addClass("loading");
      }

      //funzione di callback per retrieveILikeResults in caso di successo
      function onResultsQuerySucceeded(){

      var listItemInfo = '';
      var listItemEnumerator = allItems.getEnumerator();
      var isCurrentUserIncluded = false;
      while(listItemEnumerator.moveNext()){
      var oListItem = listItemEnumerator.get_current();
      var idUtente =  oListItem.get_item('IdUtente').toString().replace('\\','');
      var utenteCorrente = '<xsl:value-of select="$UserID"/>';

      if(idUtente == '<xsl:value-of select="$UserID" />') {
      isCurrentUserIncluded = true;
      }
      }

      if(!isCurrentUserIncluded){
      showVoteButton(true);
      }
      else{
      showVoteButton(false);
      }
      writeReportResult(allItems.get_count(), isCurrentUserIncluded);
      jQuery(".i-like-section").removeClass("loading");
      }

      //funzione di callback per retrieveILikeResults in caso di errore
      function onResultsQueryFailed(){
      //inserire uno span con una label di errore, da rendere visibile se si verifica un errore
      jQuery(".i-like-section").removeClass("loading");
      }

      function vote(){
      jQuery(".i-like-section").addClass("loading");
      var itemCreationInfo = new SP.ListItemCreationInformation();
      this.oListItem = content_feedback_list.addItem(itemCreationInfo);

      oListItem.set_item('Title', 'ILike');
      oListItem.set_item('IdUtente', '<xsl:value-of select="$UserID" />');
      oListItem.set_item('IdLista', '<xsl:value-of select="$List" />');
      oListItem.set_item('IdContenuto', '<xsl:value-of select="$SC_ID" />');
      oListItem.set_item('Data', new Date());
      oListItem.set_item('Value', 'ans-1');

      oListItem.update();

      currentClientContext.load(oListItem);

      currentClientContext.executeQueryAsync(Function.createDelegate(this, this.onVoteSucceeded), Function.createDelegate(this, this.onVoteFailed));


      }

      function onVoteSucceeded(){
      jQuery(".i-like-section").addClass("loading");
      retrieveILikeResults();
      }

      function onVoteFailed(sender, args){
      jQuery(".i-like-section").removeClass("loading");
      //inserire uno span con una label di errore, da rendere visibile se si verifica un errore
      }

      //attiva/disattiva il pulsante di voto
      function showVoteButton(show){
      if(show){
      jQuery('#voteButton').css('display','block');
      }
      else{
      jQuery('#voteButton').css('display','none');
      }
      }

      function writeReportResult(result, currentUserIncluded){

      var reportString = result == 0 ? 'Non sono ancora state espresse preferenze per questo articolo.' : 'Questo articolo piace ';


      if(currentUserIncluded){
      if(result==1){
      reportString += 'a te.';
      }
      else if (result == 2){
      reportString += 'a te e ad un\'altra persona.';
      }
      else{
      reportString += 'a te e ad altre ' + result + ' persone.';
      }
      }
      else{
      if(result==1){
      reportString += 'ad una persona.';
      }
      else if (result > 1){
      reportString += 'a ' + result + ' persone.';
      }
      }
      if(result > 0){
      jQuery('#feedback-report').css('color','green');
      }
      else{
      jQuery('#feedback-report').css('color','#909CB7');
      }

      jQuery('#feedback-report').html(reportString);

      }

      ExecuteOrDelayUntilScriptLoaded(initEnvironment, 'SP.js');
      ExecuteOrDelayUntilScriptLoaded(retrieveILikeResults, 'SP.js');

    </script>

  </xsl:template>
  
</xsl:stylesheet>
