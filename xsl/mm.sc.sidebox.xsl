<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
  <xsl:import href="/_layouts/xsl/main.xsl"/>
  <xsl:import href="/_layouts/xsl/mm.common.xsl"/>
	
  <xsl:output method="html" indent="no"/>
  <xsl:decimal-format NaN=""/>
  
  <xsl:variable name="mm_automode">0</xsl:variable>
  <xsl:variable name="abstract-max-chars" select="20" />
  
  <!-- Parameters Binded - BEGIN-->
  <xsl:param name="section_title" />
  <!-- Parameters Binded - END-->

  <xsl:template match="/">
    <xsl:call-template name="mm" />
  </xsl:template>

  <xsl:template name="mm">
    <xsl:variable name="Rows" select="/dsQueryResponse/Rows/Row"/>
    <xsl:variable name="mm_RowCount" select="count($Rows)"/>
    <xsl:variable name="RowLimit" select="6" />
    <xsl:variable name="mm_IsEmpty" select="$mm_RowCount = 0"/>
    <xsl:variable name="tipologia_scheda_contenuto" select="$XmlDefinition/Query/Where//Value[../FieldRef[@Name='tipologia_scheda_contenuto']]"/>

    <xsl:variable name="section_title">
      <xsl:choose>
        <xsl:when test="$tipologia_scheda_contenuto = 'News'">News</xsl:when>
        <xsl:when test="$tipologia_scheda_contenuto = 'Scheda Contenuto'">Schede Contenuto</xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   
    <xsl:choose>
      <xsl:when test="$mm_IsEmpty">
        <!--<xsl:call-template name="mm.empty">
			<xsl:with-param name="section_title" select="$section_title"/>        
		</xsl:call-template>-->
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mm.body">
          <xsl:with-param name="Rows" select="$Rows"/>
          <xsl:with-param name="mm_RowCount" select="$mm_RowCount" />
          <xsl:with-param name="tipologia_scheda_contenuto" select="$tipologia_scheda_contenuto" />
	  <xsl:with-param name="section_title" select="$section_title"/>  
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="mm.body">
    <xsl:param name="Rows"/>
    <xsl:param name="mm_RowCount" />
    <xsl:param name="tipologia_scheda_contenuto" />
    <xsl:param name="section_title" />
   
    <xsl:for-each select="$Rows">
      <xsl:call-template name="mm.itemview" />
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="mm.itemview">

    <!-- definisce l'url per la raccolta documenti-->
    <xsl:variable name="doc-library-href">
		<xsl:choose>
			<xsl:when test="@raccolta_documenti != ''">
				<xsl:value-of select="substring-before(substring-after(@raccolta_documenti,'href=&quot;'),'&quot;')"/>
			</xsl:when>
			<xsl:otherwise>		
			</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
    <!-- definisce l'url per la raccolta immagini-->
	<xsl:variable name="images-library-href">
		<xsl:choose>
			<xsl:when test="@raccolta_immagini != ''">
				<xsl:value-of select="substring-before(substring-after(@raccolta_immagini,'href=&quot;'),'&quot;')"/>
			</xsl:when>
			<xsl:otherwise>		
			</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
    <!-- definisce l'url di base per la pagina di dettaglio-->
	<xsl:variable name="base-detail-url-and-querystring">
		/SitePages/dettaglio_sc.aspx?SC_ID&#61;<xsl:value-of select="@ID" />&amp;area&#61;<xsl:value-of select="@Area" />
	</xsl:variable>
    <!-- definisce la parte di querystring relativa alla galleria documenti-->
	<xsl:variable name="doc-library-querystring-part">
		<xsl:choose>
			<xsl:when test="@raccolta_documenti != ''">
				&amp;doc_folder&#61;<xsl:value-of select="$doc-library-href" />				
			</xsl:when>
			<xsl:otherwise>
			
			</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
    <!-- definisce la parte di querystring relativa alla galleria immagini-->
	<xsl:variable name="images-library-querystring-part">
		<xsl:choose>
			<xsl:when test="@raccolta_immagini != ''">
				&amp;img_folder&#61;<xsl:value-of select="$images-library-href" />				
			</xsl:when>
			<xsl:otherwise>
			
			</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
    <!-- definisce l'url completo alla pagina di dettaglio (url + querystring)-->
	  <xsl:variable name="complete-detail-url-and-querystring">
		<xsl:value-of select="concat(concat($base-detail-url-and-querystring,$doc-library-querystring-part),$images-library-querystring-part)" />
    </xsl:variable>
  
    <!-- restituisce la classe per il colore dell'area cui appartiene il contenuto -->
    <xsl:variable name="color-class">
      <xsl:choose>
        <xsl:when test="contains(@Area, 'azienda')">
          <xsl:value-of select="$va-color-class"/>
        </xsl:when>
        <xsl:when test="contains(@Area, 'mestiere')">
          <xsl:value-of select="$inm-color-class"/>
        </xsl:when>
        <xsl:when test="contains(@Area, 'Strumenti')">
          <xsl:value-of select="$st-color-class"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>   

    <a href="{$complete-detail-url-and-querystring}">
        <xsl:choose>
      <xsl:when test="@tipologia_scheda_contenuto='News'">
        <div class="sidebox-sc-container">
            <!--<xsl:call-template name="mm.news.sidebox" />-->
			<xsl:call-template name="mm.sc.sidebox" />
        </div> 
      </xsl:when>
      <xsl:when test="@tipologia_scheda_contenuto='Scheda Contenuto'">
        <div class="sidebox-sc-container">
            <xsl:call-template name="mm.sc.sidebox" />
        </div>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mm.sidebox.blog" />
      </xsl:otherwise>
    </xsl:choose>               
	</a>
  </xsl:template>
  
  <xsl:template name="mm.empty">
    <xsl:param name="section_title" />
    <xsl:variable name="mm_ViewEmptyText">Nessun contenuto trovato.</xsl:variable>
    
    <div style="border-left: 3px solid #A6B2C8; font-size: 14px; font-family: Arial Narrow; padding-left: 16px; color: #333; text-transform: uppercase; height: 16px; font-weight: bold; margin-bottom: 20px;">
      <xsl:value-of select="$section_title"/>
    </div>        

    <table border="0" width="100%">
      <tr>
        <td class="ms-vb">
          <xsl:value-of select="$mm_ViewEmptyText"/>
        </td>
      </tr>
    </table>
  </xsl:template>
  

  <!-- *****************************************-->
  <!-- *****************************************-->
  <!-- *************** NEWS ********************-->
  <!-- *****************************************-->
  <!-- *****************************************-->
  
  <!--<xsl:template name="mm.news.sidebox">
	<xsl:variable name="subtitle-length">
	  <xsl:choose>
		<xsl:when test="@immagine != ''">30</xsl:when>
		<xsl:otherwise>40</xsl:otherwise>
	  </xsl:choose>
   </xsl:variable>
    <div style="color:white; font-family:verdana; font-size: 12px; position: relative; padding: 4px;">
      <div style="font-size: 12px; font-weight: bold;">
        <xsl:choose>
          <xsl:when test="string-length(@Title) &gt; 20">
            <xsl:value-of select="concat(substring(@Title, 1, 20), '...')" disable-output-escaping="yes"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@Title"/>
          </xsl:otherwise>
        </xsl:choose>
      </div>
      <xsl:choose>
        <xsl:when test="@immagine != ''">
          <xsl:variable name="img-src">
            <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
          </xsl:variable>
          <img src="{$img-src}" style="float: left; margin-right: 10px; max-width:82px; max-height:63px;"/>
          <div style="float:left; width: 100px; font-size: 11px;">
              <xsl:choose>
                <xsl:when test="string-length(@abstract) &gt; $abstract-max-chars">
                    <xsl:value-of select="concat(substring(@abstract, 1, $abstract-max-chars), '...')" disable-output-escaping="yes"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@abstract" disable-output-escaping="yes"/>
                </xsl:otherwise>
              </xsl:choose>
            </div>
        </xsl:when>
        <xsl:otherwise>
          <div style="width: 100%;">
            <xsl:choose>
              <xsl:when test="string-length(@abstract) &gt; 100">
                <p style="font-size: 11px;">
                  <xsl:value-of select="concat(substring(@abstract, 1, 100), '...')" disable-output-escaping="yes"/>
                </p>
              </xsl:when>
              <xsl:otherwise>
                <p>
                  <xsl:value-of select="@abstract" disable-output-escaping="yes"/>
                </p>
              </xsl:otherwise>
            </xsl:choose>
          </div>
        </xsl:otherwise>
      </xsl:choose>
    </div>
  </xsl:template>-->
  
  <!-- *****************************************-->
  <!-- *****************************************-->
  <!-- ********** SCHEDE CONTENUTO *************-->
  <!-- *****************************************-->
  <!-- *****************************************-->

  <xsl:template name="mm.sc.sidebox">
		<xsl:variable name="subtitle-length">
		  <xsl:choose>
			<xsl:when test="@immagine != ''">30</xsl:when>
			<xsl:otherwise>40</xsl:otherwise>
		  </xsl:choose>
	   </xsl:variable> 

      <div>
      <xsl:choose>
        <xsl:when test="@immagine != ''">		
	  
          <xsl:variable name="img-src">
            <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
          </xsl:variable>

		  <img src="{$img-src}" style="float: left; margin-right: 10px; max-width:100px; max-height:77px; display: table-cell;"/>

		  <div style="float:left; width: 100px;">
			<div class="title">
			  <xsl:call-template name="cutText">
				<xsl:with-param name="text" select="@Title" />
				<xsl:with-param name="limit" select="20" />
			  </xsl:call-template>
			</div>
			
			<xsl:if test="@subtitle != ''">
			  <div class="sub-title">
				<xsl:call-template name="cutText">
				  <xsl:with-param name="text" select="@subtitle" />
				  <xsl:with-param name="limit" select="$subtitle-length" />
				</xsl:call-template>
			  </div>
			</xsl:if>
         
          </div>
        </xsl:when>
        <xsl:otherwise>
          <div style="width: 100%;">
            <div style="font-size: 12px; font-weight: bold;">
              <xsl:value-of select="@Title"/>
            </div>
            <div>
              <xsl:choose>
                <xsl:when test="string-length(@abstract) &gt; 100">
                  <p style="font-size: 11px;">
                    <xsl:value-of select="concat(substring(@abstract, 1, 100), '...')" disable-output-escaping="yes"/>
                  </p>
                </xsl:when>
                <xsl:otherwise>
                  <p>
                    <xsl:value-of select="@abstract" disable-output-escaping="yes"/>
                  </p>
                </xsl:otherwise>
              </xsl:choose>
            </div>
          </div>
        </xsl:otherwise>
      </xsl:choose>
    </div>
  </xsl:template>

  <!-- *****************************************-->
  <!-- *****************************************-->
  <!-- **************** BLOG *******************-->
  <!-- *****************************************-->
  <!-- *****************************************-->

  <xsl:template name="mm.sidebox.blog">

    <xsl:variable name="firma">
      <xsl:choose>
        <xsl:when test="@firma. != ''">
          <xsl:value-of select="substring-after(@firma.,';#')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@Author.title"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
   
  <!-- Quando � presente la firma, recupero l'icona (se esiste), altrimenti visualizzo una immagine generica (per ora) -->
    <xsl:variable name="user-image">
      <xsl:choose>
        <xsl:when test="@firma. != ''">
          <xsl:choose>
            <xsl:when test="@icona != ''">
              <xsl:value-of select="@icona"/>  
            </xsl:when>
            <xsl:otherwise>generic-user.png</xsl:otherwise>
          </xsl:choose>  
        </xsl:when>
        <xsl:otherwise>generic-user.png</xsl:otherwise>       
      </xsl:choose>
    </xsl:variable>
    
    <div style="font-family:verdana; font-size: 12px; color: #333333; margin-bottom: 10px;">
      <div style="background: url('/Style Library/mm/img/baloon-top.png') no-repeat; width:235px; height:7px;"/>
      <div style="background: url('/Style Library/mm/img/baloon-middle.png') repeat-y; width:235px;">
        <div style="padding: 10px 20px 10px 20px;">
          <div style="font-size: 13px; font-weight: bold;margin-bottom: 15px;">
            <xsl:value-of select="@Title"/>
          </div>
          <xsl:if test="@immagine != ''" >
            <xsl:variable name="img-src">
              <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
            </xsl:variable>
            <img src="{$img-src}" width="100%"/>
          </xsl:if>
          <div>
            <xsl:choose>
              <xsl:when test="string-length(@abstract) &gt; 120">
                <xsl:value-of select="concat(substring(@abstract, 1, 120), '...')" disable-output-escaping="yes"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@abstract" disable-output-escaping="yes"/>
              </xsl:otherwise>
            </xsl:choose>
          </div>
        </div>
        <div style="position:relative; right: -128px; color:#999999; font-size: 11px;">
          <xsl:call-template name="correctDate">
            <xsl:with-param name="date" select="@Modified" />
            <xsl:with-param name="format" select="'dd MMMM yyyy'" />
          </xsl:call-template>
        </div>
      </div>
      <div style="background: url('/Style Library/mm/img/baloon-bottom.png') no-repeat; width:235px; height:20px;" />
      <div style="padding-left: 15px; margin-top: 5px; display: table;">
        <img src="/SiteCollectionImages/immagini-firme/{$user-image}" width="36px" height="36px" style="border: 3px solid #E9EAEA;"/>
        <div style="display:table-cell; vertical-align: middle; padding-left: 15px; font-family: Arial; font-size: 12px; font-weight: bold; color: #333333;">
          <xsl:value-of select="$firma"/>
        </div>
      </div>
    </div>
  </xsl:template>

  <!-- ************************************************-->
  <!-- ************************************************-->
  <!-- ********** COMMON TEMPLATES ********************-->
  <!-- ************************************************-->
  <!-- ************************************************-->

  <xsl:template name="correctDate">
    <xsl:param name="date" />
    <xsl:param name="format" />

    <xsl:variable name="a" select="substring(string($date),4,3)" />
    <xsl:variable name="b" select="substring(string($date),1,3)" />
    <xsl:variable name="c" select="substring(string($date),7,4)" />
    <xsl:variable name="correctedModifiedDate" select="concat($a,$b,$c)" />

    <xsl:value-of select="ddwrt:FormatDateTime($correctedModifiedDate, 1040,  $format)" />
  </xsl:template>

     <!-- taglia un testo se (e solo se) supera un certo numero di caratteri-->
  <xsl:template name="cutText">
    
    <xsl:param name="text" />
    <xsl:param name="limit" />

    <xsl:choose>
      <xsl:when test="string-length($text) &gt; $limit">
        <xsl:value-of select="concat(substring($text, 1, $limit), '...')" disable-output-escaping="yes"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" disable-output-escaping="yes"/>
       </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
