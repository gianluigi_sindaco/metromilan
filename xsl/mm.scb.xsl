<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
  <xsl:import href="/_layouts/xsl/main.xsl"/>

  <xsl:output method="html" indent="no"/>
  <xsl:decimal-format NaN=""/>

  <xsl:variable name="mm_automode">0</xsl:variable>

  <!-- Parameters Binded - BEGIN-->
  <xsl:param name="section_title" />
  <!-- Parameters Binded - END-->

  <xsl:template match="/">
	<xsl:call-template name="mm"/>
  </xsl:template>

  <xsl:template name="mm">

    <xsl:variable name="Rows" select="/dsQueryResponse/Rows/Row"/>
    <xsl:variable name="mm_RowCount" select="count($Rows)"/>
    <xsl:variable name="mm_IsEmpty" select="$mm_RowCount = 0"/>
    <xsl:variable name="tipologia_scheda_contenuto" select="$XmlDefinition/Query/Where//Value[../FieldRef[@Name='tipologia_scheda_contenuto_breve']]"/>
		
    <xsl:choose>
      <xsl:when test="$mm_IsEmpty">
        <xsl:call-template name="mm.empty"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mm.body">
          <xsl:with-param name="Rows" select="$Rows"/>
          <xsl:with-param name="tipologia_scheda_contenuto" select="$tipologia_scheda_contenuto" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="mm.body">
    <xsl:param name="Rows"/>
    <xsl:param name="tipologia_scheda_contenuto" />

    <xsl:variable name="section_title">
      <xsl:choose>
        <xsl:when test="$tipologia_scheda_contenuto = 'Link'">Link Utili</xsl:when>
        <xsl:when test="$tipologia_scheda_contenuto = 'Banner'">In primo piano</xsl:when>
		<xsl:when test="$tipologia_scheda_contenuto = 'Highlight'">Highlights</xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!--<div style="border-left: 3px solid #A6B2C8; font-size: 14px; font-family: Arial Narrow; padding-left: 16px; color: #333; text-transform: uppercase; height: 16px; font-weight: bold; margin-bottom: 20px;">
      <xsl:value-of select="$section_title"/>
    </div>-->

    <xsl:if test="$tipologia_scheda_contenuto = 'Link'">
      <div style="width: 220px;">
        <xsl:for-each select="$Rows">
          <xsl:call-template name="mm.scb.link.item" />
        </xsl:for-each>
      </div>
    </xsl:if>
    <xsl:if test="$tipologia_scheda_contenuto = 'Banner'">
		<div style="width: 220px;">
			<xsl:for-each select="$Rows">
			  <xsl:call-template name="mm.scb.banner.item" />
			</xsl:for-each>
		</div>
    </xsl:if>
    <xsl:if test="$tipologia_scheda_contenuto = 'highlight'">

    </xsl:if>


  </xsl:template>

  <!-- *****************************************-->
  <!-- *****************************************-->
  <!-- *************** LINK ********************-->
  <!-- *****************************************-->
  <!-- *****************************************-->

	<xsl:template name="mm.scb.link.item">
		<xsl:if test="@link_interno != '' or @link_esterno != ''">
			    <div style="border-bottom: 1px solid #E9EAEA; padding-bottom: 5px; height: 50px;">
				  <div style="float:left; width: 10%; height: 50px; background:url('/Style Library/mm/img/link-arrow.png') no-repeat center center; margin-right: 10px;" />
				  <div style="float:left; width: 80%; height: 50px; position: relative; display: table;">
					<div style="">
						<div style="font-family: arial; color: #586d98; font-size: 13px; font-weight: bold; margin-bottom:10px; margin-top: 5px;">
							<xsl:value-of select="@Title"/>
						</div>
					</div>
					<xsl:if test="@link_interno != ''">
						<xsl:variable name="border">
							<xsl:choose>
								<xsl:when test="@link_esterno != ''">float:left; padding-right: 5px; border-right: 1px dotted gray;</xsl:when>
								<xsl:otherwise>float:left; padding-right: 5px;</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>

						<div style="{$border}">
						  <a href="{@link_interno}" style="font-family: arial; color: #586d98; font-size: 11px;" target="_blank">
							link interno
						  </a>
						</div>
					</xsl:if>
					<xsl:if test="@link_esterno != ''">
						<div style="float:left; padding-left: 5px;">
						  <a href="{@link_esterno}" style="font-family: arial; color: #586d98; font-size: 11px; " target="_blank">
							link esterno
						  </a>
						</div>
					</xsl:if>
				  </div>
				</div>
		</xsl:if>
  </xsl:template>
  
  	<!--<xsl:template name="mm.scb.link.item">
		<xsl:if test="@link_interno != '' or @link_esterno != ''">
			    <div style="border-bottom: 1px solid #E9EAEA; padding-bottom: 5px; height:35px;">
				  <div style="float:left; width: 10%; height: 35px; background:url('/Style Library/mm/img/link-arrow.png') no-repeat center 8px; margin-right: 10px;" />
				  <div style="float:left; width: 80%; height: 35px; position: relative; display: table;">
					  <xsl:choose>
						<xsl:when test="@link_interno != ''">
						  <a href="{@link_interno}" target="_blank">
							<div style="font-family: arial; color: #586d98; font-size: 13px; font-weight: bold; margin-bottom:0px; margin-top: 5px;">
								<xsl:value-of select="@Title"/>
							</div>
						  </a>					
						</xsl:when>
						<xsl:otherwise>
							<div style="font-family: arial; color: #586d98; font-size: 13px; font-weight: bold; margin-bottom:0px; margin-top: 5px;">
								<xsl:value-of select="@Title"/>
							</div>					
						</xsl:otherwise>
					  </xsl:choose>

					<xsl:if test="@link_esterno != ''">
						<div style="float:left;">
						  <a href="{@link_esterno}" style="font-family: arial; color: #586d98; font-size: 11px; " target="_blank">
							link esterno
						  </a>
						</div>
					</xsl:if>
				  </div>
				</div>
		</xsl:if>
    </xsl:template>-->

  <!-- *****************************************-->
  <!-- *****************************************-->
  <!-- *************** BANNER ******************-->
  <!-- *****************************************-->
  <!-- *****************************************-->

  <xsl:template name="mm.scb.banner.item">
    <xsl:if test="@immagine != ''" >
      <xsl:variable name="img-src">
        <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="@link_interno != ''">
          <a href="{@link_interno}" target="_blank">
            <div style="width:100%; height: 115px; background:url({$img-src}) no-repeat 0 0; margin-bottom: 5px; border-bottom: 1px solid #E9EAEA; padding-bottom: 5px;" />
          </a>
        </xsl:when>
        <xsl:otherwise>
          <div style="width:100%; height: 115px; background:url({$img-src}) no-repeat 0 0; margin-bottom: 5px; border-bottom: 1px solid #E9EAEA; padding-bottom: 5px;" />  
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if> 
  </xsl:template>
  
  <xsl:template name="mm.empty">
    <xsl:variable name="mm_ViewEmptyText"></xsl:variable>

    <table border="0" width="100%">
      <tr>
        <td class="ms-vb">
          <xsl:value-of select="$mm_ViewEmptyText"/>
        </td>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>
