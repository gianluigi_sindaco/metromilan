<xsl:stylesheet xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" version="1.0" exclude-result-prefixes="xsl msxsl ddwrt"
                xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:asp="http://schemas.microsoft.com/ASPNET/20"
                xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal" ddwrt:oob="true">

  <xsl:import href="/_layouts/xsl/main.xsl"/>
  <xsl:import href="/_layouts/xsl/mm.common.xsl"/>
  
  <xsl:output method="html" indent="no"/>
  <xsl:decimal-format NaN=""/>
  
  <xsl:variable name="mm_automode">0</xsl:variable>
  <xsl:variable name="abstract-max-chars" select="150" />
  
  <!-- Parameters Binded - BEGIN-->
  <xsl:param name="dvt_firstrow">1</xsl:param>
  <xsl:param name="nextpagedata" >1</xsl:param>
  <xsl:param name="apos">&apos;</xsl:param>
  <!-- Parameters Binded - END-->

  <xsl:template match="/">
    <xsl:call-template name="mm" />
  </xsl:template>

  <xsl:template name="mm">
    <xsl:variable name="tipologia_scheda_contenuto" select="$XmlDefinition/Query/Where//Value[../FieldRef[@Name='tipologia_scheda_contenuto']]"/>
    <xsl:variable name="Rows" select="/dsQueryResponse/Rows/Row"/>
    <xsl:variable name="RowCount" select="count($Rows)"/>
    <xsl:variable name="RowLimit">
		<xsl:choose>
			<xsl:when test="$tipologia_scheda_contenuto = 'News'">8</xsl:when>
			<xsl:when test="$tipologia_scheda_contenuto = 'Scheda Contenuto'">12</xsl:when>
			<xsl:when test="$tipologia_scheda_contenuto = 'Item Elenco'">3</xsl:when>
			<xsl:otherwise>8</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
    <xsl:variable name="FirstRow" select="$dvt_firstrow" />
    <xsl:variable name="LastRow" select="$FirstRow + $RowLimit - 1" />
    <xsl:variable name="IsEmpty" select="$RowCount = 0"/>

    <xsl:variable name="section_title">
      <xsl:choose>
        <xsl:when test="$tipologia_scheda_contenuto = 'News'">News</xsl:when>
        <xsl:when test="$tipologia_scheda_contenuto = 'Scheda Contenuto'">Schede Contenuto</xsl:when>
		<xsl:when test="$tipologia_scheda_contenuto = 'Item Elenco'">Elenchi</xsl:when>
        <xsl:otherwise>Blog</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$IsEmpty">
        <xsl:call-template name="mm.empty">
    		<xsl:with-param name="section_title" select="$section_title" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="mm.body">
          <xsl:with-param name="Rows" select="$Rows"/>
          <xsl:with-param name="FirstRow" select="$FirstRow" />
          <xsl:with-param name="LastRow" select="$LastRow" />
          <xsl:with-param name="RowLimit" select="$RowLimit" />
          <xsl:with-param name="RowCount" select="$RowCount" />
          <xsl:with-param name="tipologia_scheda_contenuto" select="$tipologia_scheda_contenuto" />
		  <xsl:with-param name="section_title" select="$section_title" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    
    <xsl:call-template name="mm.commandfooter">
      <xsl:with-param name="Rows" select="$Rows" />
      <xsl:with-param name="FirstRow" select="$FirstRow" />
      <xsl:with-param name="LastRow" select="$LastRow" />
      <xsl:with-param name="RowLimit" select="$RowLimit" />
      <xsl:with-param name="RowCount" select="$RowCount" />
      <xsl:with-param name="RealLastRow" select="number(ddwrt:NameChanged('',-100))" />
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="mm.body">
    <xsl:param name="Rows"/>
    <xsl:param name="FirstRow" />
    <xsl:param name="LastRow" />
    <xsl:param name="RowLimit" />
    <xsl:param name="RowCount" />
    <xsl:param name="tipologia_scheda_contenuto" />
    <xsl:param name="section_title" />  
    
    <xsl:variable name="NumPages" select="ceiling($RowCount div $RowLimit)"/>
    
    <xsl:variable name="Num_Items_For_Row">
      <xsl:choose>
        <xsl:when test="$tipologia_scheda_contenuto = 'News'">2</xsl:when>
        <xsl:when test="$tipologia_scheda_contenuto = 'Scheda Contenuto'">3</xsl:when>
		<xsl:when test="$tipologia_scheda_contenuto = 'Item Elenco'">3</xsl:when>
        <xsl:otherwise>2</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
       
    <div style="width: 700px;">
      <!--<div class="section-title">
        <xsl:value-of select="$section_title"/>
      </div>-->
      <xsl:for-each select="$Rows">
          <xsl:if test="(position() &gt;= $FirstRow and position() &lt;= $LastRow)">
            <xsl:call-template name="mm.itemview" />
          </xsl:if>

          <xsl:if test = "(position() &gt;= $FirstRow and position() &lt;= $LastRow and (position() mod $Num_Items_For_Row = 0))">
            <div style="clear: both; width: 100%; height: 1px;" />
          </xsl:if>
        </xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template name="mm.itemview">

    <!-- definisce l'url per la raccolta documenti-->
    <xsl:variable name="doc-library-href">
      <xsl:choose>
        <xsl:when test="@raccolta_documenti != ''">
          <xsl:value-of select="substring-before(substring-after(@raccolta_documenti,'href=&quot;'),'&quot;')"/>
        </xsl:when>
        <xsl:otherwise>   
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <!-- definisce l'url per la raccolta immagini-->
  <xsl:variable name="images-library-href">
    <xsl:choose>
      <xsl:when test="@raccolta_immagini != ''">
        <xsl:value-of select="substring-before(substring-after(@raccolta_immagini,'href=&quot;'),'&quot;')"/>
      </xsl:when>
      <xsl:otherwise>   
      </xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    
    <!-- definisce l'url di base per la pagina di dettaglio-->
  <xsl:variable name="base-detail-url-and-querystring">/SitePages/dettaglio_sc.aspx?SC_ID&#61;<xsl:value-of select="@ID" />&amp;area&#61;<xsl:value-of select="@Area" /></xsl:variable>
    <!-- definisce la parte di querystring relativa alla galleria documenti-->
  <xsl:variable name="doc-library-querystring-part">
    <xsl:choose>
      <xsl:when test="@raccolta_documenti != ''">&amp;doc_folder&#61;<xsl:value-of select="$doc-library-href" /></xsl:when>
      <xsl:otherwise>
      
      </xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    <!-- definisce la parte di querystring relativa alla galleria immagini-->
  <xsl:variable name="images-library-querystring-part">
    <xsl:choose>
      <xsl:when test="@raccolta_immagini != ''">&amp;img_folder&#61;<xsl:value-of select="$images-library-href" /></xsl:when>
      <xsl:otherwise>
      
      </xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    
	<!-- definisce l'url completo alla pagina di dettaglio (url + querystring)-->
    <xsl:variable name="complete-detail-url-and-querystring">
    <xsl:value-of select="concat(concat($base-detail-url-and-querystring,$doc-library-querystring-part),$images-library-querystring-part)" />
    </xsl:variable>
	
	<xsl:variable name="url-to-item-list">
	  <xsl:choose>
		<xsl:when test="contains(@Area,'azienda')">vita_d_azienda.aspx?category=<xsl:value-of select="@IdCategoria"/></xsl:when>
		<xsl:when test="contains(@Area,'Strumenti')">strumenti.aspx?category=<xsl:value-of select="@IdCategoria"/></xsl:when>
		<xsl:when test="contains(@Area,'mestiere')">il_nostro_mestiere.aspx?category=<xsl:value-of select="@IdCategoria"/></xsl:when>
	  </xsl:choose>
	</xsl:variable>

    <!-- restituisce la classe per il colore dell'area cui appartiene il contenuto -->
    <xsl:variable name="color-class">
      <xsl:choose>
        <xsl:when test="contains(@Area, 'azienda')">
          <xsl:value-of select="$va-color-class"/>
        </xsl:when>
        <xsl:when test="contains(@Area, 'mestiere')">
          <xsl:value-of select="$inm-color-class"/>
        </xsl:when>
        <xsl:when test="contains(@Area, 'Strumenti')">
          <xsl:value-of select="$st-color-class"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="@tipologia_scheda_contenuto='News'">
        <div class="row-news-item {$color-class}">
          <div>
            <a href="{$complete-detail-url-and-querystring}" style="text-decoration: none;">
              <xsl:choose>
                <xsl:when test="@stile_box = 'background immagine'">
                  <xsl:call-template name="mm.news.background.image" />
                </xsl:when>
                <xsl:when test="@stile_box = 'miniatura immagine'">
                  <xsl:call-template name="mm.news.limited.image" />
                </xsl:when>
                <xsl:when test="@stile_xbox = 'solo testo'">
                  <xsl:call-template name="mm.news.no.image" />
                </xsl:when>
                <xsl:otherwise>
                 <xsl:call-template name="mm.news.no.image" />
                </xsl:otherwise>
              </xsl:choose>
            </a>
          </div>
        </div>
      </xsl:when>
      <xsl:when test="@tipologia_scheda_contenuto='Scheda Contenuto'">
        <div class="row-sc-item {$color-class}">
          <div>
            <a href="{$complete-detail-url-and-querystring}">
              <xsl:choose>
                <xsl:when test="@stile_box = 'background immagine'">                 
                  <xsl:call-template name="mm.sc.background.image" />
                </xsl:when>
                <xsl:when test="@stile_box = 'miniatura immagine'">
                  <xsl:call-template name="mm.sc.limited.image" />
                </xsl:when>
                <xsl:when test="@stile_box = 'solo testo'">
                  <xsl:call-template name="mm.sc.no.image" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:call-template name="mm.sc.no.image" />
                </xsl:otherwise>
              </xsl:choose>
            </a>
          </div>
        </div>
      </xsl:when>
	  <xsl:when test="@tipologia_scheda_contenuto='Item Elenco'">	  	  
        <div class="row-sc-item {$color-class}">
          <div>
            
              <xsl:choose>
                <xsl:when test="@stile_box = 'background immagine'">                 
                  <xsl:call-template name="mm.ie.background.image" >
				    <xsl:with-param name="complete-detail-url-and-querystring" select="$complete-detail-url-and-querystring"/>
					<xsl:with-param name="url-to-item-list" select="$url-to-item-list"/>
				  </xsl:call-template>
                </xsl:when>
                <xsl:when test="@stile_box = 'miniatura immagine'">
                  <xsl:call-template name="mm.ie.limited.image">
					<xsl:with-param name="complete-detail-url-and-querystring" select="$complete-detail-url-and-querystring"/>
					<xsl:with-param name="url-to-item-list" select="$url-to-item-list"/>
				  </xsl:call-template>
                </xsl:when>
                <xsl:when test="@stile_box = 'solo testo'">
                  <xsl:call-template name="mm.ie.no.image" >
				    <xsl:with-param name="complete-detail-url-and-querystring" select="$complete-detail-url-and-querystring"/>
					<xsl:with-param name="url-to-item-list" select="$url-to-item-list"/>
				  </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:call-template name="mm.ie.no.image">
				    <xsl:with-param name="complete-detail-url-and-querystring" select="$complete-detail-url-and-querystring"/>
					<xsl:with-param name="url-to-item-list" select="$url-to-item-list"/>
				  </xsl:call-template>
                </xsl:otherwise>
              </xsl:choose>
          </div>
        </div>
      </xsl:when>
      <xsl:otherwise>
        <div class="row-news-item">
          <div>
            <a href="{$complete-detail-url-and-querystring}">
              <xsl:call-template name="mm.blog.background.image" />
            </a>
          </div>
        </div>
      </xsl:otherwise>
    </xsl:choose>               
  </xsl:template>
  
  <xsl:template name="mm.empty">
    <xsl:param name="section_title" />  
    <xsl:variable name="mm_ViewEmptyText"></xsl:variable>
    <!--<div class="section-title">
      <xsl:value-of select="$section_title"/>
    </div>-->
        
    <table border="0" width="700px;">
      <tr>
        <td class="ms-vb">
          <xsl:value-of select="$mm_ViewEmptyText"/>
        </td>
      </tr>
    </table>
  </xsl:template>
  
  <xsl:template name="mm.commandfooter">
    
    <xsl:param name="Rows" />
    <xsl:param name="FirstRow" />
    <xsl:param name="LastRow" />
    <xsl:param name="RowLimit" />
    <xsl:param name="RowCount" />
    <xsl:param name="RealLastRow" />
    
    <table cellspacing="0" cellpadding="4" border="0" width="100%">
      <tr>
        <xsl:if test="$FirstRow &gt; 1 or $nextpagedata">
          <xsl:call-template name="mm.navigation">
            <xsl:with-param name="Rows" select="$Rows" />
            <xsl:with-param name="FirstRow" select="$FirstRow" />
            <xsl:with-param name="LastRow" select="$LastRow" />
            <xsl:with-param name="RowLimit" select="$RowLimit" />
            <xsl:with-param name="RowCount" select="$RowCount" />
            <xsl:with-param name="RealLastRow" select="$RealLastRow" />
          </xsl:call-template>
        </xsl:if>
      </tr>
    </table>
  </xsl:template>
  
  <xsl:template name="mm.navigation">
    <xsl:param name="Rows" />
    <xsl:param name="FirstRow" />
    <xsl:param name="LastRow" />
    <xsl:param name="RowLimit" />
    <xsl:param name="RowCount" />
    <xsl:param name="RealLastRow" />
    <xsl:variable name="PrevRow">

      <xsl:choose>
        <xsl:when test="$FirstRow - $RowLimit &lt; 1">1</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$FirstRow - $RowLimit" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="LastRowValue">
      <xsl:choose>
        <xsl:when test="$LastRow &gt; $RealLastRow">
          <xsl:value-of select="$LastRow"></xsl:value-of>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$RealLastRow"></xsl:value-of>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="NextRow">
      <xsl:value-of select="$LastRowValue + 1"></xsl:value-of>
    </xsl:variable>
    <td nowrap="nowrap" class="ms-paging" align="left">

      <xsl:variable name="NumPages" select="ceiling($RowCount div $RowLimit)"/>

      <xsl:if test="$NumPages &gt; 1">
        Pag.
        <xsl:text xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" ddwrt:nbsp-preserve="yes" disable-output-escaping="yes">&amp;nbsp;</xsl:text>

        <xsl:for-each select="$Rows">
          <xsl:if test="(position() mod $RowLimit) = 1">
            <xsl:choose>
              <xsl:when test="position() = $FirstRow">
                <xsl:value-of select="((position() - 1) div $RowLimit) + 1"/>
              </xsl:when>
              <xsl:otherwise>
                <a>
                  <xsl:attribute name="href">
                    javascript: <xsl:value-of xmlns:xsl="http://www.w3.org/1999/XSL/Transform" select="ddwrt:GenFireServerEvent(concat('dvt_firstrow={',position(),'}'))"/>;
                  </xsl:attribute>
                  <xsl:value-of select="((position() - 1) div $RowLimit) + 1"/>
                </a>
              </xsl:otherwise>
            </xsl:choose>

            <xsl:text xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" ddwrt:nbsp-preserve="yes" disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </xsl:if>
        </xsl:for-each>

      </xsl:if>
    </td>
  </xsl:template>

  <!-- *****************************************-->
  <!-- *****************************************-->
  <!-- *************** NEWS ********************-->
  <!-- *****************************************-->
  <!-- *****************************************-->
  
  <!-- Template per news con background immagine -->
  
 <xsl:template name="mm.news.background.image">

    <!-- lunghezza in caratteri dell'abstract -->

    <xsl:variable name="abstract-length">
      <xsl:choose>
        <xsl:when test="@subtitle != ''">110</xsl:when>
        <xsl:otherwise>100</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="subtitle-length">
      <xsl:choose>
        <xsl:when test="@abstract != ''">90</xsl:when>
        <xsl:otherwise>100</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- immagine fascetta -->

    <xsl:variable name="strip-image-class">
      <xsl:choose>
        <xsl:when test="contains(@Area, 'azienda')">azure-strip</xsl:when>
        <xsl:when test="contains(@Area, 'mestiere')">red-strip</xsl:when>
        <xsl:when test="contains(@Area, 'Strumenti')">gray-strip</xsl:when>
      </xsl:choose>
    </xsl:variable>

    <div class="sc-box-news" >
      
      <xsl:if test="@immagine != ''">
        <xsl:variable name="img-src">
          <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
        </xsl:variable>
        <!-- immagine di sfondo-->
        <!--<div style="background: url('{$img-src}') no-repeat scroll transparent; width:330px; height: 260px;" />-->
		<div style="width:330px; height: 260px; overflow: hidden;" >
			<img src="{$img-src}" style="min-width: 330px; min-height:260px;"/>
		</div>
        <!-- fascetta dell'area, in alto a sinistra -->
        <div class="{$strip-image-class}" />
      </xsl:if>

      <div class="transparent">
        
  		<div class="title">
        <xsl:call-template name="cutText">
          <xsl:with-param name="text" select="@Title" />
          <xsl:with-param name="limit" select="50" />
        </xsl:call-template>
      </div>

      <xsl:if test="@subtitle != ''">
        <div class="sub-title">
          <xsl:call-template name="cutText">
            <xsl:with-param name="text" select="@subtitle" />
            <xsl:with-param name="limit" select="$subtitle-length" />
          </xsl:call-template>
        </div>
      </xsl:if>
  		
  		<xsl:if test="@abstract != ''">
  			<div class="abstract">
  			  <xsl:call-template name="cutText">
  				<xsl:with-param name="text" select="@abstract" />
  				<xsl:with-param name="limit" select="$abstract-length" />
  			  </xsl:call-template>
  			</div>
  		</xsl:if>
	
        <div class="date">
          <xsl:call-template name="correctDate">
            <xsl:with-param name="date" select="@Modified" />
            <xsl:with-param name="format" select="'dd MMMM yyyy'" />
          </xsl:call-template>
        </div>
      </div>
    </div>
  </xsl:template>

<xsl:template name="mm.news.limited.image">

    <!-- lunghezza in caratteri dell'abstract -->
    <xsl:variable name="abstract-length">
      <xsl:choose>
        <xsl:when test="@subtitle != ''">110</xsl:when>
        <xsl:otherwise>100</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="subtitle-length">
      <xsl:choose>
        <xsl:when test="@abstract != ''">90</xsl:when>
        <xsl:otherwise>100</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <div class="sc-box-news">
      <div style="width: 90%; padding: 20px;">
        <div class="title">
          <xsl:call-template name="cutText">
            <xsl:with-param name="text" select="@Title" />
            <xsl:with-param name="limit" select="90" />
          </xsl:call-template>

        </div>

        <xsl:if test="@subtitle != ''">
          <div class="sub-title">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@subtitle" />
              <xsl:with-param name="limit" select="$subtitle-length" />
            </xsl:call-template>
          </div>
        </xsl:if>
        
        <!--<xsl:if test="@immagine != ''" >
          <xsl:variable name="img-src">
            <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
          </xsl:variable>
          <img src="{$img-src}" style="float: left; margin-right: 10px; max-height:90px; max-width:150px;"/>
        </xsl:if>-->
		
		<xsl:variable name="img-src">
		  <xsl:choose>
			<xsl:when test="@IdCategoria != 12 and @IdCategoria != 13 and @IdCategoria != 15">
				<xsl:if test="@immagine != ''" >
					<xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
				</xsl:if>	
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="@IconaCategoria"/>
			</xsl:otherwise>
		  </xsl:choose>
		</xsl:variable>
		
		<xsl:if test="$img-src != ''" >
			<img src="{$img-src}" style="float: left; margin-right: 10px; max-height:90px; max-width:150px;"/>
		</xsl:if>
		
		<xsl:if test="abstract != ''">
  			 <p class="abstract">
  				<xsl:call-template name="cutText">
  				  <xsl:with-param name="text" select="@abstract" />
  				  <xsl:with-param name="limit" select="$abstract-length" />
  				</xsl:call-template>
  			</p>
  		</xsl:if>

        
        <div class="date">
          <xsl:call-template name="correctDate">
            <xsl:with-param name="date" select="@Modified" />
            <xsl:with-param name="format" select="'dd MMMM yyyy'" />
          </xsl:call-template>
        </div>
        </div>
      </div>
  </xsl:template>

    <!-- Template per news senza immagine -->
  <xsl:template name="mm.news.no.image">

     <!-- lunghezza in caratteri dell'abstract -->
    <xsl:variable name="abstract-length">
      <xsl:choose>
        <xsl:when test="@subtitle != ''">110</xsl:when>
        <xsl:otherwise>150</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="subtitle-length">
      <xsl:choose>
        <xsl:when test="@abstract != ''">90</xsl:when>
        <xsl:otherwise>100</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <div class="sc-box-news">
      <div style="width: 90%; padding: 20px;">
        
        <div class="title">
          <xsl:call-template name="cutText">
            <xsl:with-param name="text" select="@Title" />
            <xsl:with-param name="limit" select="50" />
          </xsl:call-template>
        </div>

        <xsl:if test="@subtitle != ''">
          <div class="sub-title">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@subtitle" />
              <xsl:with-param name="limit" select="$subtitle-length" />
            </xsl:call-template>
          </div>
        </xsl:if>

        <div class="abstract">
          <xsl:call-template name="cutText">
            <xsl:with-param name="text" select="@abstract" />
            <xsl:with-param name="limit" select="$abstract-length" />
          </xsl:call-template>
        </div>

        <div class="date">
          <xsl:call-template name="correctDate">
            <xsl:with-param name="date" select="@Modified" />
            <xsl:with-param name="format" select="'dd MMMM yyyy'" />
          </xsl:call-template>
        </div>
      </div>
    </div>
  </xsl:template>


  <!-- ************************************************-->
  <!-- ************************************************-->
  <!-- **************** SCHEDE CONTENUTO **************-->
  <!-- ************************************************-->
  <!-- ************************************************-->
  
  <xsl:template name="mm.sc.background.image">

    <!-- lunghezza in caratteri dell'abstract -->
    <xsl:variable name="abstract-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">50</xsl:when>
        <xsl:otherwise>40</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="subtitle-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">50</xsl:when>
        <xsl:otherwise>40</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <div class="sc-box-sc">
        
        <div class="title">
          <xsl:call-template name="cutText">
            <xsl:with-param name="text" select="@Title" />
            <xsl:with-param name="limit" select="35" />
          </xsl:call-template>
        </div>

        <xsl:if test="@subtitle != ''">
          <div class="sub-title">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@subtitle" />
              <xsl:with-param name="limit" select="$subtitle-length" />
            </xsl:call-template>
          </div>
        </xsl:if>

        <xsl:if test="@immagine != ''" >
          <xsl:variable name="img-src">
            <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
          </xsl:variable>
          <div style="width:100%; height: 65px; background:url({$img-src}) no-repeat center center; margin-bottom: 5px;">
          </div>
        </xsl:if>
		        
        <xsl:if test="@abstract !=''">
          <div class="abstract">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@abstract" />
              <xsl:with-param name="limit" select="$abstract-length" />
            </xsl:call-template>
          </div>          
        </xsl:if>
        
        <div class="date">
          <xsl:call-template name="correctDate">
            <xsl:with-param name="date" select="@Modified" />
            <xsl:with-param name="format" select="'dd MMMM yyyy'" />
          </xsl:call-template>
        </div>

    </div>
  </xsl:template>

  <xsl:template name="mm.sc.limited.image">

    <!-- lunghezza in caratteri dell'abstract -->
    <xsl:variable name="abstract-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">110</xsl:when>
        <xsl:otherwise>130</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
	
    <xsl:variable name="subtitle-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">45</xsl:when>
        <xsl:otherwise>50</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <div class="sc-box-sc">
        
        <div class="title">
          <xsl:call-template name="cutText">
            <xsl:with-param name="text" select="@Title" />
            <xsl:with-param name="limit" select="30" />
          </xsl:call-template>
        </div>

        <xsl:if test="@subtitle != ''">
          <div class="sub-title">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@subtitle" />
              <xsl:with-param name="limit" select="$subtitle-length" />
            </xsl:call-template>
          </div>
        </xsl:if>
       
        <!--<xsl:if test="@immagine != ''" >
          <xsl:variable name="img-src">
            <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
          </xsl:variable>
          <img src="{$img-src}" style="float: left; margin-right: 10px; max-width: 80px; max-height: 100px;"/>
        </xsl:if>-->
		
		<xsl:variable name="img-src">
		  <xsl:choose>
			<xsl:when test="@IdCategoria != 12 and @IdCategoria != 13 and @IdCategoria != 15">
				<xsl:if test="@immagine != ''" >
					<xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
				</xsl:if>	
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="@IconaCategoria"/>
			</xsl:otherwise>
		  </xsl:choose>
		</xsl:variable>
				
		<xsl:if test="$img-src != ''" >
		  <img src="{$img-src}" style="float: left; margin-right: 10px; max-width: 80px; max-height: 100px;"/>
		</xsl:if>		
        
        <xsl:if test="@abstract !=''">
          <p class="abstract">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@abstract" />
              <xsl:with-param name="limit" select="$abstract-length" />
            </xsl:call-template>
          </p>
        </xsl:if>

        <div class="date">
          <xsl:call-template name="correctDate">
            <xsl:with-param name="date" select="@Modified" />
            <xsl:with-param name="format" select="'dd MMMM yyyy'" />
          </xsl:call-template>
        </div>
    </div>
  </xsl:template>
  
  <xsl:template name="mm.sc.no.image">

    <!-- lunghezza in caratteri dell'abstract -->
    <xsl:variable name="abstract-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">50</xsl:when>
        <xsl:otherwise>40</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="subtitle-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">50</xsl:when>
        <xsl:otherwise>40</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

     <div class="sc-box-sc">
        
        <div class="title">
          <xsl:call-template name="cutText">
            <xsl:with-param name="text" select="@Title" />
            <xsl:with-param name="limit" select="30" />
          </xsl:call-template>
        </div>

        <xsl:if test="@subtitle != ''">
          <div class="sub-title">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@subtitle" />
              <xsl:with-param name="limit" select="$subtitle-length" />
            </xsl:call-template>
          </div>
        </xsl:if>
              
        <xsl:if test="@abstract !=''">
          <div class="abstract">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@abstract" />
              <xsl:with-param name="limit" select="$abstract-length" />
            </xsl:call-template>
          </div>          
        </xsl:if>

        <div class="date">
          <xsl:call-template name="correctDate">
            <xsl:with-param name="date" select="@Modified" />
            <xsl:with-param name="format" select="'dd MMMM yyyy'" />
          </xsl:call-template>
        </div>
    </div>
  </xsl:template>
  

  
  <!-- ************************************************-->
  <!-- ************************************************-->
  <!-- ******************* ITEM ELENCO ****************-->
  <!-- ************************************************-->
  <!-- ************************************************-->
  <xsl:template name="mm.ie.background.image">
	
	<xsl:param name="url-to-item-list" />
	<xsl:param name="complete-detail-url-and-querystring" />
    <!-- lunghezza in caratteri dell'abstract -->
    <xsl:variable name="abstract-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">50</xsl:when>
        <xsl:otherwise>40</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="subtitle-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">50</xsl:when>
        <xsl:otherwise>40</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <div class="sc-box-sc">
        <a href="{$complete-detail-url-and-querystring}">
        <div class="title">
          <xsl:call-template name="cutText">
            <xsl:with-param name="text" select="@Title" />
            <xsl:with-param name="limit" select="35" />
          </xsl:call-template>
        </div>

        <xsl:if test="@subtitle != ''">
          <div class="sub-title">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@subtitle" />
              <xsl:with-param name="limit" select="$subtitle-length" />
            </xsl:call-template>
          </div>
        </xsl:if>

        <xsl:if test="@immagine != ''" >
          <xsl:variable name="img-src">
            <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
          </xsl:variable>
          <div style="width:100%; height: 65px; background:url({$img-src}) no-repeat center center; margin-bottom: 5px;">
          </div>
        </xsl:if>
        
        <xsl:if test="@abstract !=''">
          <div class="abstract">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@abstract" />
              <xsl:with-param name="limit" select="$abstract-length" />
            </xsl:call-template>
          </div>          
        </xsl:if>
	  </a>
  	  <div class="item-list-link">
		<a href="{$url-to-item-list}">altri in questa categoria</a>
      </div>
 
        <div class="date">
          <xsl:call-template name="correctDate">
            <xsl:with-param name="date" select="@Modified" />
            <xsl:with-param name="format" select="'dd MMMM yyyy'" />
          </xsl:call-template>
        </div>

    </div>
  </xsl:template>

  <xsl:template name="mm.ie.limited.image">

	<xsl:param name="url-to-item-list" />
	<xsl:param name="complete-detail-url-and-querystring" />
    
	<!-- lunghezza in caratteri dell'abstract -->
    <xsl:variable name="abstract-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">110</xsl:when>
        <xsl:otherwise>130</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="subtitle-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">45</xsl:when>
        <xsl:otherwise>50</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
	
    <div class="sc-box-sc">
        <a href="{$complete-detail-url-and-querystring}">
			<div class="title">
			  <xsl:call-template name="cutText">
				<xsl:with-param name="text" select="@Title" />
				<xsl:with-param name="limit" select="30" />
			  </xsl:call-template>
			</div>

			<xsl:if test="@subtitle != ''">
			  <div class="sub-title">
				<xsl:call-template name="cutText">
				  <xsl:with-param name="text" select="@subtitle" />
				  <xsl:with-param name="limit" select="$subtitle-length" />
				</xsl:call-template>
			  </div>
			</xsl:if>
		   
			<!--<xsl:if test="@immagine != ''" >
			  <xsl:variable name="img-src">
				<xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
			  </xsl:variable>
			  <img src="{$img-src}" style="float: left; margin-right: 10px; max-width: 80px; max-height: 100px;"/>
			</xsl:if>-->
			
		<xsl:variable name="img-src">
		  <xsl:choose>
			<xsl:when test="@IdCategoria != 12 and @IdCategoria != 13 and @IdCategoria != 15">
				<xsl:if test="@immagine != ''" >
					<xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
				</xsl:if>	
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="@IconaCategoria"/>
			</xsl:otherwise>
		  </xsl:choose>
		</xsl:variable>
		
		<xsl:if test="$img-src != ''" >
		  <img src="{$img-src}" style="float: left; margin-right: 10px; max-width: 80px; max-height: 100px;"/>
		</xsl:if>			
			
			<xsl:if test="@abstract !=''">
			  <p class="abstract">
				<xsl:call-template name="cutText">
				  <xsl:with-param name="text" select="@abstract" />
				  <xsl:with-param name="limit" select="$abstract-length" />
				</xsl:call-template>
			  </p>
			</xsl:if>
		</a>
	  <div class="item-list-link">
		<a href="{$url-to-item-list}">altri in questa categoria</a>
	  </div>

		  
		
        <div class="date">
          <xsl:call-template name="correctDate">
            <xsl:with-param name="date" select="@Modified" />
            <xsl:with-param name="format" select="'dd MMMM yyyy'" />
          </xsl:call-template>
        </div>
    </div>
  </xsl:template>
  
  <xsl:template name="mm.ie.no.image">
	
	<xsl:param name="url-to-item-list" />
	<xsl:param name="complete-detail-url-and-querystring" />

    <!-- lunghezza in caratteri dell'abstract -->
    <xsl:variable name="abstract-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">50</xsl:when>
        <xsl:otherwise>40</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="subtitle-length">
      <xsl:choose>
        <xsl:when test="@immagine != ''">50</xsl:when>
        <xsl:otherwise>40</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

     <div class="sc-box-sc">
        <a href="{$complete-detail-url-and-querystring}">
        <div class="title">
          <xsl:call-template name="cutText">
            <xsl:with-param name="text" select="@Title" />
            <xsl:with-param name="limit" select="30" />
          </xsl:call-template>
        </div>

        <xsl:if test="@subtitle != ''">
          <div class="sub-title">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@subtitle" />
              <xsl:with-param name="limit" select="$subtitle-length" />
            </xsl:call-template>
          </div>
        </xsl:if>
              
        <xsl:if test="@abstract !=''">
          <div class="abstract">
            <xsl:call-template name="cutText">
              <xsl:with-param name="text" select="@abstract" />
              <xsl:with-param name="limit" select="$abstract-length" />
            </xsl:call-template>
          </div>          
        </xsl:if>
		</a>
  	    <div class="item-list-link">
			<a href="{$url-to-item-list}">altri in questa categoria</a>
        </div>
		
        <div class="date">
          <xsl:call-template name="correctDate">
            <xsl:with-param name="date" select="@Modified" />
            <xsl:with-param name="format" select="'dd MMMM yyyy'" />
          </xsl:call-template>
        </div>
    </div>
  </xsl:template>



  <!-- *****************************************-->
  <!-- *****************************************-->
  <!-- *************** BLOG ********************-->
  <!-- *****************************************-->
  <!-- *****************************************-->

  <!-- Template per blog -->

<xsl:template name="mm.blog.background.image">
    <xsl:variable name="firma">
      <xsl:choose>
        <xsl:when test="@Firma. != ''">
          <xsl:value-of select="substring-after(@Firma.,';#')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@Author.title"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Quando � presente la firma, recupero l'icona (se esiste), altrimenti visualizzo una immagine generica (per ora) -->
    <xsl:variable name="user-image">
      <xsl:choose>
        <xsl:when test="@Firma. != ''">
          <xsl:choose>
            <xsl:when test="@icona != ''">
              <xsl:value-of select="@icona"/>
            </xsl:when>
            <xsl:otherwise>generic-user.png</xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>generic-user.png</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="local-abstract-lenght">
      <xsl:choose>
        <xsl:when test="@immagine != ''">
          <xsl:value-of select="$abstract-max-chars"/> 
        </xsl:when>
        <xsl:otherwise>120</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <div class="sc-box-blog">
      <div>
        <div style="font-size: 15px; font-weight: bold;margin-bottom: 15px;">
          <xsl:value-of select="@Title"/>
        </div>
        <div style="margin-top: 5px; display: table; margin-bottom: 10px;">
          <img src="/SiteCollectionImages/immagini-firme/{$user-image}" width="16px" height="16px" style="border: 3px solid #E9EAEA;"/>
          <div class="firma">
            Autore: <xsl:value-of select="$firma"/>
          </div>
        </div>
        <xsl:if test="@immagine != ''">
          <xsl:variable name="img-src">
            <xsl:value-of select="substring-before(substring-after(@immagine,'src=&quot;'),'&quot;')"/>
          </xsl:variable>
          <div style="background: url({$img-src}) no-repeat 0 0; width: 100%; height: 110px; margin-bottom: 10px;">

          </div>
        </xsl:if>
        <div>
          <xsl:choose>
            <xsl:when test="string-length(@abstract) &gt; $local-abstract-lenght">
              <xsl:value-of select="concat(substring(@abstract, 1, $local-abstract-lenght), '...')" disable-output-escaping="yes"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="@abstract" disable-output-escaping="yes"/>
            </xsl:otherwise>
          </xsl:choose>
        </div>
      </div>
      <div style="height: 20px; font-size: 11px; margin-top: 15px; position:absolute; bottom: 7px;">
        <xsl:call-template name="correctDate">
          <xsl:with-param name="date" select="@Modified" />
          <xsl:with-param name="format" select="'dd MMMM yyyy'" />
        </xsl:call-template>
      </div>
    </div>
  </xsl:template>
  
 
  <!-- ************************************************-->
  <!-- ************************************************-->
  <!-- ********** COMMON TEMPLATES ********************-->
  <!-- ************************************************-->
  <!-- ************************************************-->

  <!-- corregge la formattazione della data-->
  <xsl:template name="correctDate">
    <xsl:param name="date" />
    <xsl:param name="format" />

    <xsl:variable name="a" select="substring(string($date),4,3)" />
    <xsl:variable name="b" select="substring(string($date),1,3)" />
    <xsl:variable name="c" select="substring(string($date),7,4)" />
    <xsl:variable name="correctedModifiedDate" select="concat($a,$b,$c)" />
    
    <xsl:value-of select="ddwrt:FormatDateTime($correctedModifiedDate, 1040,  $format)" />
  </xsl:template>

   <!-- taglia un testo se (e solo se) supera un certo numero di caratteri-->
  <xsl:template name="cutText">
    
    <xsl:param name="text" />
    <xsl:param name="limit" />

    <xsl:choose>
      <xsl:when test="string-length($text) &gt; $limit">
        <xsl:value-of select="concat(substring($text, 1, $limit), '...')" disable-output-escaping="yes"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" disable-output-escaping="yes"/>
       </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
